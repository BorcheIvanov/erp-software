using System.Linq;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;
using Xunit;

namespace ERP.Tests.IntegrationTests
{
    public class TestDocumentsRepository: TestBase
    {
        public TestDocumentsRepository()
        {
            base.AddDocuments();
        }

        [Fact]
        public async void GetAllDocumentsShouldReturnNotNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 21;
            //When
            var res = await dr.GetAllDocumentsAsync(userCompanyID);
            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public async void GetAllDocumentsShouldFilterCompanyID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 2;
            //When
            var res = await dr.GetAllDocumentsAsync(userCompanyID);
            //Then
            Assert.Single(res);
        }

        [Fact]
        public async void GetAllDocumentsClientShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = (await dr.GetAllDocumentsAsync(userCompanyID)).FirstOrDefault();
            //Then
            Assert.NotNull(res.Client);
        }

        [Fact]
        public async void GetAllDocumentsDocumentTypeShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = (await dr.GetAllDocumentsAsync(userCompanyID)).FirstOrDefault();
            //Then
            Assert.NotNull(res.DocumentType);
        }

        [Fact]
         public async void GetAllDocumentsWithDetailsShouldReturnNotNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 21;
            //When
            var res = await dr.GetAllDocumentsWithDetailsAsync(userCompanyID);
            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public async void GetAllDocumentsWithDetailsShouldFilterCompanyID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 2;
            //When
            var res = await dr.GetAllDocumentsWithDetailsAsync(userCompanyID);
            //Then
            Assert.Single(res);
        }

        [Fact]
        public async void GetAllDocumentsWithDetailsClientShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = (await dr.GetAllDocumentsWithDetailsAsync(userCompanyID)).FirstOrDefault();
            //Then
            Assert.NotNull(res.Client);
        }

        [Fact]
        public async void GetAllDocumentsWithDetailsDocumentTypeShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = (await dr.GetAllDocumentsWithDetailsAsync(userCompanyID)).FirstOrDefault();
            //Then
            Assert.NotNull(res.DocumentType);
        }

        [Fact]
        public async void GetAllDocumentsWithDetailsDocumentDetailsShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = (await dr.GetAllDocumentsWithDetailsAsync(userCompanyID)).FirstOrDefault();
            //Then
            Assert.NotNull(res.DocumentDetails);
        }

        [Fact]
        public void GetDocumentShouldRetrunNotNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public void GetDocumentShouldReturnDocumentObject()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.IsType<Document>(res);
        }

        [Fact]
        public void GetDocumentClientShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.Client);
        }

        [Fact]
        public void GetDocumentDocumentTypeShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentType);
        }

        [Fact]
        public void GetDocumentDocumentDetailsShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails);
        }

        [Fact]
        public void GetDocumentDocumentDetailsItemShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails[0].Item);
        }

        [Fact]
        public void GetDocumentDocumentDetailsItemTypeShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails[0].Item.ItemType);
        }

        [Fact]
        public void GetDocumentDocumentDetailsVatClassShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails[0].VatClass);
        }

        [Fact]
        public void GetDocumentDocumentDetailsItemPriceShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails[0].ItemPrice);
        }

        [Fact]
        public void GetDocumentDocumentDetailsItemPackingShouldNotBeNull()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            var userCompanyID = 1;
            //When
            var res = dr.GetDocument(1, userCompanyID);
            //Then
            Assert.NotNull(res.DocumentDetails[0].Item.Packing);
        }

    }
}