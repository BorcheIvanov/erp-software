using System;
using System.Collections.Generic;
using System.Linq;
using ERP.Infrastructure;
using ERP.DomainModel;
using Microsoft.EntityFrameworkCore;

namespace ERP.Tests.IntegrationTests
{
    public class TestBase
    {
        private readonly DbContextOptions<ERP.Infrastructure.ApplicationDbContext> _options;
        protected ApplicationDbContext _context;

        public TestBase()
        {
            _options = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase(databaseName: $"inMemoryDb_{Guid.NewGuid()}")
                .Options;

            _context = new ApplicationDbContext(_options);       
        }

        internal void AddDocuments()
        {
            if (!_context.Documents.Any())
            {
                _context.Documents.Add(new Document
                {
                    ID = 1,
                    CompanyID = 1,
                    DocumentType = new DocumentType
                    {
                        ID = 1,
                        Name = "docType"
                    },
                    Client = new Client
                    {
                        ID = 1,
                        Name = "client"
                    },
                    Company = new Company
                    {
                        ID = 1
                    },
                    DocumentDetails = new List<DocumentDetails>
                    {
                        new DocumentDetails
                        {
                            ID = 1,
                            DocumentID = 1,
                            Item = new Item
                            {
                                Name = "item",
                                ItemType = new ItemType
                                {
                                    ID = 1,
                                    Name = "ItemType"
                                },
                                Packing = new ItemPacking
                                {
                                    ID = 1,
                                    Name = "firts"
                                }
                            },
                            VatClass = new VatClass
                            {
                                ID = 1,
                                Name = "bla"
                            },
                            ItemPrice = new ItemPrice
                            {
                                ID = 1,
                            }
                        }
                    }
                });
                
                _context.Documents.Add(new Document
                {
                    ID = 2,
                    CompanyID = 1,
                    DocumentType = new DocumentType
                    {
                        ID = 2,
                        Name = "docType"
                    },
                    Client = new Client
                    {
                        ID = 2,
                        Name = "client"
                    },
                    Company = new Company
                    {
                        ID = 1
                    },
                    DocumentDetails = new List<DocumentDetails>
                    {
                        new DocumentDetails
                        {
                            ID = 2,
                            DocumentID = 2,
                            Item = new Item
                            {
                                Name = "item",
                                ItemType = new ItemType
                                {
                                    ID = 2,
                                    Name = "ItemType"
                                },
                                Packing = new ItemPacking
                                {
                                    ID = 2,
                                    Name = "firts"
                                }
                            },
                            VatClass = new VatClass
                            {
                                ID = 2,
                                Name = "bla"
                            },
                            ItemPrice = new ItemPrice
                            {
                                ID = 2,
                            }
                        }
                    }
                });

                _context.Documents.Add(new Document
                {
                    ID = 3,
                    CompanyID = 2,
                    DocumentType = new DocumentType
                    {
                        ID = 3,
                        Name = "docType"
                    },
                    Client = new Client
                    {
                        ID = 3,
                        Name = "client"
                    },
                    Company = new Company
                    {
                        ID = 2
                    },
                    DocumentDetails = new List<DocumentDetails>
                    {
                        new DocumentDetails
                        {
                            ID = 3,
                            DocumentID = 3,
                            Item = new Item
                            {
                                ID = 3,
                                Name = "item",
                                ItemType = new ItemType
                                {
                                    ID = 3,
                                    Name = "ItemType"
                                },
                                Packing = new ItemPacking
                                {
                                    ID = 3,
                                    Name = "firts"
                                }
                            },
                            VatClass = new VatClass
                            {
                                ID = 3,
                                Name = "bla"
                            },
                            ItemPrice = new ItemPrice
                            {
                                ID = 3,
                            }
                        }
                    }
                });
                _context.SaveChanges();
            }
        }

        internal void AddItemTypes()
        {
            if(!_context.ItemTypes.Any())
            {
                Console.WriteLine("Adding Item Types");
                _context.Add(new ItemType
                {
                    Name = "type1",
                    Active = true,
                    AffectingQuantity = true,
                    CompanyID = 1
                });
                _context.Add(new ItemType
                {
                    Name = "type2",
                    Active = true,
                    AffectingQuantity = true,
                    CompanyID = 1
                });
                _context.Add(new ItemType
                {
                    Name = "type3",
                    Active = false,
                    AffectingQuantity = true,
                    CompanyID = 1
                });
                _context.Add(new ItemType
                {
                    Name = "type4",
                    Active = true,
                    AffectingQuantity = true,
                    CompanyID = 2
                });
                _context.SaveChanges();
            }
        }

        internal void AddVatClass()
        {
            if (!_context.VatClass.Any())
            {
                 Console.WriteLine("Adding Vat Class");
                _context.VatClass.Add(new VatClass
                {
                    Name = "vat1",
                    Active = true,
                    CompanyID = 1,
                    Value = 15
                });
                _context.VatClass.Add(new VatClass
                {
                    Name = "vat2",
                    Active = false,
                    CompanyID = 1,
                    Value = 15
                });
                _context.VatClass.Add(new VatClass
                {
                    Name = "vat3",
                    Active = true,
                    CompanyID = 2,
                    Value = 15
                });
                _context.VatClass.Add(new VatClass
                {
                    Name = "vat4",
                    Active = true,
                    CompanyID = 1,
                    Value = 18
                });
                _context.SaveChanges();
            }
        }

        internal void AddClients()
        {
            if(!_context.Clients.Any())
            {
                 Console.WriteLine("Adding Clients");
                _context.Clients.Add(new Client
                {
                    Name = "Client 1",
                    Active = true,
                    CompanyID = 1
                });
                _context.Clients.Add(new Client
                {
                    Name = "Client 2",
                    Active = true,
                    CompanyID = 1
                });
                _context.Clients.Add(new Client
                {
                    Name = "Client 3",
                    Active = false,
                    CompanyID = 1
                });
                _context.Clients.Add(new Client
                {
                    Name = "Client 34",
                    Active = true,
                    CompanyID = 2
                });
                _context.Clients.Add(new Client
                {
                    Name = "Client 35",
                    Active = true,
                    CompanyID = 2
                });
                _context.SaveChanges();
            }
        }

        internal void AddItems()
        {
            if (!_context.Items.Any())
            {
                _context.Items.Add(new Item
                {
                    Code = "001",
                    Name = "small table",
                    Active = true,
                    CompanyID = 1
                });

                _context.Items.Add(new Item
                {
                    Code = "002",
                    Name = "big table",
                    Active = true,
                    CompanyID = 1
                });

                _context.Items.Add(new Item
                {
                    Code = "213",
                    Name = "mid table",
                    Active = true,
                    CompanyID = 1
                });

                _context.Items.Add(new Item
                {
                    Code = "001",
                    Name = "glass",
                    Active = false,
                    CompanyID = 1
                });

                _context.Items.Add(new Item
                {
                    Code = "011",
                    Name = "stool",
                    Active = true,
                    CompanyID = 2
                });

                _context.SaveChanges();
            }
        }

        internal void AddDocumentTypes()
        {
            if (!_context.DocumentTypes.Any())
            {
                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "one",
                    CompanyID = 1,
                    Active = true
                });

                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "two",
                    CompanyID = 1,
                    Active = true
                });

                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "trhee",
                    CompanyID = 1,
                    Active = false
                });

                _context.DocumentTypes.Add(new DocumentType
                {
                    Name = "onetwo",
                    CompanyID = 2,
                    Active = true
                });

                _context.SaveChanges();
            }
        }
    }
    
}