using Xunit;
using System.Linq;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;

namespace ERP.Tests.IntegrationTests
{
    public class TestVatClassRepository: TestBase
    {
        public TestVatClassRepository()
        {
            base.AddVatClass();
        }

        [Fact]
        public async void TestGetActiveVatCLassListShouldReturnOnlyActive()
        {
            //Given
            var vcr = new VatClassRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await vcr.GetActiveVatClass(userCompanyID)).ToList();
            //Then
            Assert.Equal(2, res.Count);
        }

        [Fact]
        public async void TestGetActiveVatCLassListShouldReturnFilterCompany()
        {
            //Given
            var vcr = new VatClassRepository(_context);
            //When
            var userCompanyID = 2;
            var res = await vcr.GetActiveVatClass(userCompanyID);
            //Then
            Assert.Single(res);
        }

        [Fact]
        public async void TestGetActiveVatCLassListShouldReturnNotNull()
        {
            //Given
            var vcr = new VatClassRepository(_context);
            //When
            var userCompanyID = 22312131;
            var res = await vcr.GetActiveVatClass(userCompanyID);
            //Then
            Assert.NotNull(res);
        }
    }
}