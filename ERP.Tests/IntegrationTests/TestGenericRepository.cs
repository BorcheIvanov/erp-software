using System;
using System.Threading.Tasks;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;
using Xunit;

namespace ERP.Tests.IntegrationTests
{
    public class TestGenericRepository: TestBase
    {
        public TestGenericRepository()
        {
            base.AddDocuments();
        }

        [Fact]
        public void TestGetByIDShouldReturnFilterID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 1;
            var doc = dr.GetByID(id, userCompanyID);
            //Then
            Assert.Equal(id, doc.ID);
        }

        [Fact]
        public void TestGetByIDShouldReturnFilterCompanyID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 2;
            var doc = dr.GetByID(id, userCompanyID);
            //Then
            Assert.Equal(userCompanyID, doc.CompanyID);
        }

        [Fact]
        public void TestGetByIDShouldRThrowIfIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 123121; 
            Exception ex = Assert.Throws<Exception>(() => dr.GetByID(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Message);
        }

        [Fact]
        public void TestGetByIDShouldRThrowIfCompanyIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 23211;
            var id = 1; 
            Exception ex = Assert.Throws<Exception>(() => dr.GetByID(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Message);
        }

        [Fact]
        public void TestGetByIDShouldRThrowIfIDAndCompanyIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1231231;
            var id = 123121; 
            Exception ex = Assert.Throws<Exception>(() => dr.GetByID(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Message);
        }

        [Fact]
        public async void AsyncTestGetByIDShouldReturnFilterID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 1;
            var doc = await dr.GetByIDAsync(id, userCompanyID);
            //Then
            Assert.Equal(id, doc.ID);
        }

        [Fact]
        public async void AsyncTestGetByIDShouldReturnFilterCompanyID()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 2;
            var doc = await dr.GetByIDAsync(id, userCompanyID);
            //Then
            Assert.Equal(userCompanyID, doc.CompanyID);
        }

        [Fact]
        public void AsyncTestGetByIDShouldRThrowIfIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1;
            var id = 123121; 
            Task<Exception> ex = Assert.ThrowsAsync<Exception>(async () => await dr.GetByIDAsync(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Result.Message);
        }

        [Fact]
        public void AsyncTestGetByIDShouldRThrowIfCompanyIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 23211;
            var id = 1; 
            Task<Exception> ex = Assert.ThrowsAsync<Exception>(async () => await dr.GetByIDAsync(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Result.Message);
        }

        [Fact]
        public void AsyncTestGetByIDShouldRThrowIfIDAndCompanyIDNotExist()
        {
            //Given
            var dr = new DocumentsRepository(_context);
            //When
            var userCompanyID = 1231231;
            var id = 123121; 
            Task<Exception> ex = Assert.ThrowsAsync<Exception>(async () => await dr.GetByIDAsync(id, userCompanyID));
            //Then
            Assert.Equal("404 NotFound", ex.Result.Message);
        }
    }
}