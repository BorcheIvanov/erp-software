using Xunit;
using System.Linq;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;

namespace ERP.Tests.IntegrationTests
{
    public class TestItemTypesRepository: TestBase
    {
        public TestItemTypesRepository()
        {
            base.AddItemTypes();
        }

        [Fact]
        public async void GetActiveItemTypesShouldReturnOnlyActive()
        {
            //Given
            var itr = new ItemTypesRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await itr.GetActiveItemTypes(userCompanyID)).ToList();
            //Then
            Assert.Equal(2, res.Count);
        }

        [Fact]
        public async void GetActiveItemTypesShouldFilterCompany()
        {
            //Given
            var itr = new ItemTypesRepository(_context);
            //When
            var userCompanyID = 2;
            var res = await itr.GetActiveItemTypes(userCompanyID);
            //Then
            Assert.Single(res);
        }

        [Fact]
        public async void GetActiveItemTypesShouldReturnNotNull()
        {
            //Given
            var itr = new ItemTypesRepository(_context);
            //When
            var userCompanyID = 1123211;
            var res = (await itr.GetActiveItemTypes(userCompanyID)).ToList();
            //Then
            Assert.NotNull(res);
        }
    }
}