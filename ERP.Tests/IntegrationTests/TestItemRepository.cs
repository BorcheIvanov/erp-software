using Xunit;
using System.Linq;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;

namespace ERP.Tests.IntegrationTests
{
    public class TestItemRepository: TestBase
    {
        public TestItemRepository()
        {
            base.AddItems();
        }

        [Fact]
        public async void TestSeachShouldReturnNotNull()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 1;
            var res = await ir.Search(userCompanyID, "bsdfsdqwbfnkjnas");
            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public async void TestSeachShouldFilterAllItemsWhenEmptyStringPassed()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await ir.Search(userCompanyID, string.Empty)).ToList();
            //Then
            Assert.True(res.Any());
        }

        [Fact]
        public async void TestSeachShouldFilterCompanyID()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 2;
            var res = await ir.Search(userCompanyID, string.Empty);
            //Then
            Assert.Single(res);
        }

        [Fact]
        public async void TestSeachShouldFilterByCode()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await ir.Search(userCompanyID, "00")).ToList();
            //Then
            Assert.Equal(2, res.Count);
        }

        [Fact]
        public async void TestSeachShouldFilterByName()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await ir.Search(userCompanyID, "table")).ToList();
            //Then
            Assert.Equal(3, res.Count);
        }

        [Fact]
        public async void TestSeachShouldReturnAllActiveWhenEmptyStringPassed()
        {
            //Given
            var ir = new ItemRepository(_context);
            //When
            var userCompanyID = 1;
            var res = (await ir.Search(userCompanyID, "")).ToList();
            //Then
            Assert.Equal(3, res.Count);
        }
    }
}