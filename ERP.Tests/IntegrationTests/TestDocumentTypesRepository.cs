using System.Linq;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;
using Xunit;

namespace ERP.Tests.IntegrationTests
{
    public class TestDocumentTypesRepository: TestBase
    {
        public TestDocumentTypesRepository()
        {
            base.AddDocumentTypes();
        }

        [Fact]
        public async void TestGetActiveDocumentTypesShouldReturnNotNull()
        {
            //Given
            var dtr = new DocumentTypesRepository(_context);
            var userCompanyID = 3;
            //When
            var res = await dtr.GetActiveDocumentTypes(userCompanyID);
            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public async void TestGetActiveDocumentTypesShouldReturnOnlyActive()
        {
            //Given
            var dtr = new DocumentTypesRepository(_context);
            var userCompanyID = 1;
            //When
            var res = await dtr.GetActiveDocumentTypes(userCompanyID);
            //Then
            Assert.Equal(2, res.Count());
        }

        [Fact]
        public async void TestGetActiveDocumentTypesShouldFilterCompanyID()
        {
            //Given
            var dtr = new DocumentTypesRepository(_context);
            var userCompanyID = 2;
            //When
            var res = await dtr.GetActiveDocumentTypes(userCompanyID);
            //Then
            Assert.Single(res);
        }
    }
}