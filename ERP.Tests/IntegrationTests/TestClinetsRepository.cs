using System.Collections.Generic;
using Xunit;
using System.Linq;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Infrastructure.Repositories;

namespace ERP.Tests.IntegrationTests
{
    public class TestClientsRepository : TestBase
    {
        public TestClientsRepository()
        {
            base.AddClients();
        }    
    
        [Fact]
        public async void TestSearchClientShouldRetrunNotNull()
        {
            //Given
            var cr = new CLientsRepository(_context);

            //When
            var res = (await cr.Search(1, "")).ToList();

            //Then
            Assert.NotNull(res);
        }

        [Fact]
        public async void TestSearchClientShouldRetrunEmptyList()
        {
            var cr = new CLientsRepository(_context);
            var newListOfClients = new List<Client>();

            var res = (await cr.Search(1, "ClienthatDoNotExist")).ToList();
            
            Assert.Equal(newListOfClients, res);
        }

       
        [Fact]
        public async void TestSearchClientShouldRetrunOnlyActiveClients()
        {
                var cr = new CLientsRepository(_context);

                var res = (await cr.Search(1, "Client")).ToList();

                Assert.Equal(2, res.Count);
        }

        [Fact]
        public async void TestSearchClientShouldFilterCompanyID()
        {
            //Given
            var cr = new CLientsRepository(_context);

            //When
            var res = (await cr.Search(2, "Client")).ToList();

            //Then
            Assert.Equal(2, res.Count);
        }
    }    
}