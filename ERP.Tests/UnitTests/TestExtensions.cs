using System;
using System.Security.Claims;
using System.Security.Principal;
using ERP.Extensions;
using Xunit;

namespace ERP.Tests.UnitTests
{
    public class TestExtensions
    {
        [Fact]
        public void GetCompanyIdShouldReturnNumber()
        {
            //assign
            var identity = new GenericIdentity("borche");

            //act
            identity.AddClaim(new Claim("CompanyID", "1"));
            var result = IdentityExtensions.GetCompanyID(identity);

            //assert
            Assert.Equal(1, result);
        }

        [Fact]
        public void GetCompanyIdShouldTrowExceptionIfHasNoCompanyID()
        {
            //assign
            var identity = new GenericIdentity("borche");

            //act
            Exception ex = Assert.Throws<Exception>(() => IdentityExtensions.GetCompanyID(identity));
            //assert
            Assert.Equal("No CompanyID assigned!", ex.Message);
        }

        [Fact]
        public void GetCompanyIdShouldTrowExceptionIfCompanyIDisNotNumber()
        {
            //assign
            var identity = new GenericIdentity("borche");
            identity.AddClaim(new Claim("CompanyID", "abc"));
            //act
            Exception ex = Assert.Throws<Exception>(() => IdentityExtensions.GetCompanyID(identity));
            //assert
            Assert.Equal("CompanyID is not a number!", ex.Message);
        }

        [Fact]
        public void GetCompanyIdShouldTrowExceptionIfCompanyIDisEmptyString()
        {
            //assign
            var identity = new GenericIdentity("borche");
            identity.AddClaim(new Claim("CompanyID", string.Empty));
            //act
            Exception ex = Assert.Throws<Exception>(() => IdentityExtensions.GetCompanyID(identity));
            //assert
            Assert.Equal("CompanyID is not a number!", ex.Message);
        }

    }
}
