using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class DocumentTypesRepository: Repository<DocumentType>, IDocumentTypesRepository
    {
        public DocumentTypesRepository(ApplicationDbContext contex) : base(contex)
        {
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }

        public async Task<IEnumerable<DocumentType>> GetActiveDocumentTypes(int userCompanyID)
        {
            return await _context.DocumentTypes.Where(x => x.CompanyID == userCompanyID && x.Active == true)
                                               .ToListAsync();
        }

    }
}