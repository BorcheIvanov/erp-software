using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using ERP.DomainRepository;

namespace ERP.Infrastructure.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext dbContext;
        protected readonly int userCompanyID;


        public Repository(DbContext context)
        {
            dbContext = context;
        }
        public async Task<TEntity> GetByIDAsync(int id, int userCompanyID)
        {
            var entity = await dbContext.Set<TEntity>().Where(HasSameID(id))
                                                        .Where(HasSameCompanyID(userCompanyID))
                                                        .SingleOrDefaultAsync(); 

            if (entity == null)
                throw new Exception("404 NotFound");
            
            return entity;
        }

        public TEntity GetByID(int id, int userCompanyID)
        {
            var entity = dbContext.Set<TEntity>().Where(HasSameID(id))
                                                 .Where(HasSameCompanyID(userCompanyID))
                                                 .SingleOrDefault(); 

            if (entity == null)
                throw new Exception("404 NotFound");
            
            return entity;
        }

        

        public IEnumerable<TEntity> GetAll(int userCompanyID)
        {

            return dbContext.Set<TEntity>().Where(HasSameCompanyID(userCompanyID));
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync(int userCompanyID)
        {
            Expression<Func<TEntity, bool>> hasSameCompanyID = HasSameCompanyID(userCompanyID);

            return await dbContext.Set<TEntity>().Where(hasSameCompanyID).ToListAsync();
        }

        
        public TEntity FindSingle(Expression<Func<TEntity, bool>> predicate)
        {
            return dbContext.Set<TEntity>().Where(predicate).SingleOrDefault();
        }
        public async Task<TEntity> FindSingleAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await dbContext.Set<TEntity>().Where(predicate).SingleOrDefaultAsync();
        }
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return dbContext.Set<TEntity>().Where(predicate);
        }

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await dbContext.Set<TEntity>().Where(predicate).ToListAsync();
        }

        public void Add(TEntity entity)
        {
            dbContext.Set<TEntity>().Add(entity);
        }
        public void AddRange(IEnumerable<TEntity> entities)
        {
            dbContext.Set<TEntity>().AddRange(entities);
        }

        public void Remove(TEntity entity)
        {
            dbContext.Set<TEntity>().Remove(entity);
        }
        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbContext.Set<TEntity>().RemoveRange(entities);
        }

        private Expression<Func<TEntity, bool>> HasSameCompanyID(int userCompanyID)
        {
            ParameterExpression p = Expression.Parameter(typeof(TEntity), "p");
            Expression query = Expression.Equal(Expression.Property(p, "CompanyID"), Expression.Constant(userCompanyID));
            Expression<Func<TEntity, bool>> hasSameCompanyID = Expression.Lambda<Func<TEntity, bool>>(query, p);
            return hasSameCompanyID;
        }

        private Expression<Func<TEntity, bool>> HasSameID(int id)
        {
            ParameterExpression p = Expression.Parameter(typeof(TEntity), "p");
            Expression query = Expression.Equal(Expression.Property(p, "ID"), Expression.Constant(id));
            Expression<Func<TEntity, bool>> ID = Expression.Lambda<Func<TEntity, bool>>(query, p);
            return ID;
        }

        public void AddOrUpdate(TEntity entity)
        {
            var keyValues = GetPrimaryKeyValues(dbContext, entity);
            var id = keyValues.Where(x => x.Key == "ID").SingleOrDefault().Value;

            var savedEntity = dbContext.Set<TEntity>().Find(id);

            if (savedEntity != null)
            {
                dbContext.Entry<TEntity>(savedEntity).CurrentValues.SetValues(entity);
            }
            else
            {
                dbContext.Set<TEntity>().Add(entity);
            }
        }

        public IDictionary<string, object> GetPrimaryKeyValues(DbContext contex, object entity)
        {
            if (contex == null)
            {
                throw new ArgumentNullException(nameof(contex));
            }

            if (entity == null)
            {
                throw new ArgumentNullException(nameof(entity));
            }
            
            var entry = contex.Entry(entity);
            var primaryKey = entry.Metadata.FindPrimaryKey();
            var keys = primaryKey.Properties.ToDictionary(x => x.Name, x => x.PropertyInfo.GetValue(entity));

            return keys;
        }


    }
}