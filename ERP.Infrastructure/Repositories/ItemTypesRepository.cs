using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class ItemTypesRepository: Repository<ItemType>, IItemTypesRepository
    {
        public ItemTypesRepository(ApplicationDbContext contex) : base(contex)
        {
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }

        public async Task<IEnumerable<ItemType>> GetActiveItemTypes(int userCompanyID)
        {
            return await _context.ItemTypes.Where(x => x.CompanyID == userCompanyID && x.Active == true).ToListAsync();
        }
    }
}