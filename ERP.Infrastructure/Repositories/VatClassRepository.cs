using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class VatClassRepository : Repository<VatClass>, IVatClassRepository
    {
        public VatClassRepository(ApplicationDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<VatClass>> GetActiveVatClass(int userCompanyID)
        {
            return await _context.VatClass.Where(x => x.CompanyID == userCompanyID && x.Active == true).ToListAsync();
        }

        public ApplicationDbContext _context
        {
            get{return dbContext as ApplicationDbContext;}
        }
    }
}