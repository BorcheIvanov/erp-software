using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class ItemRepository : Repository<Item>, IItemRepository
    {
        public ItemRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ApplicationDbContext _context => dbContext as ApplicationDbContext;


        public async Task<IEnumerable<Item>> GetItemListWithPrices(int userCompanyID)
        {
            var items = await _context.Items
                                    .Include(item => item.ItemType)
                                    .Include(item => item.VatClass)
                                    .Include(item => item.Price)
                                    .Where(item => item.CompanyID == userCompanyID)
                                    .ToListAsync();

            foreach (var item in items)
            {
                item.ItemPrice = item.Price.LastOrDefault(p => p.DateValidFrom <= DateTime.Now && p.DateValidTo >= DateTime.Now) ?? new ItemPrice();
            }

            return items;
        }

        public async Task<Item> GetItemWithPrice(int itemID, int userCompanyID)
        {
            var item = await _context.Items
                                    .Include(i => i.ItemType)
                                    .Include(i => i.VatClass)
                                    .Include(i => i.Price)
                                    .SingleOrDefaultAsync(i => i.CompanyID == userCompanyID && i.ID == itemID);

            item.ItemPrice = item.Price.LastOrDefault(p => p.DateValidFrom <= DateTime.Now && p.DateValidTo >= DateTime.Now) ?? new ItemPrice();
            return item;
        }

        public async Task<IEnumerable<Item>> Search(int userCompanyID, string text) =>
            await _context.Items.Where(i => (i.Name.Contains(text) 
                                             || i.Code.Contains(text))
                                            && i.CompanyID == userCompanyID
                                            && i.Active == true).ToListAsync();

        public async Task<IEnumerable<Item>> GetItemsStock(int companyId)
        {
            var items = await _context.Items.Where(x => x.CompanyID == companyId).ToListAsync();

            var documents = await _context.Documents
                .Include(doc => doc.Client)
                .Include(doc => doc.DocumentType)
                .Include(doc => doc.DocumentDetails)
                .Where(doc => doc.CompanyID == companyId)
                .ToListAsync();



            var itemsWithStock = items.Select(i => new Item
            {
                Code = i.Code,
                Name = i.Name,
                Description = i.Description,
                Stock = documents.Sum(x => x.DocumentDetails.Where(d => d.ItemID == i.ID
                                                                        && d.Document.DocumentType.AffectingQuantity)
                    .Sum(xx => xx.Quantity * xx.Document.DocumentType.AffectingValue))
            });

            return itemsWithStock;
        }
    }
}