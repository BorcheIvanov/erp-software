using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class CLientsRepository: Repository<Client>, IClientsRepository
    {
        public CLientsRepository(ApplicationDbContext contex) : base(contex)
        {

        }

        public async Task<IEnumerable<Client>> Search(int userCompanyID, string text)
        {
            return await _context.Clients.Where(x => x.CompanyID == userCompanyID 
                                                && x.Name.Contains(text)
                                                && x.Active == true)
                                    .ToListAsync();
        }
        
        
        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }
    }
}