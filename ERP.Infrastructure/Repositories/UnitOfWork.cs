using System.Threading.Tasks;
using ERP.DomainRepository;
using ERP.Infrastructure;

namespace ERP.Infrastructure.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _context;

        public UnitOfWork(ApplicationDbContext context)
        {
            _context = context;
            VatClass = new VatClassRepository(_context);
            Items = new ItemRepository(_context);
            ItemPrices = new ItemPriceRepository(_context);
            Clients = new CLientsRepository(_context);
            Documents = new DocumentsRepository(_context);
            ItemTypes = new ItemTypesRepository(_context);
            DocumentTypes = new DocumentTypesRepository(_context);
            ItemPacking = new ItemPackingRepository(_context);
            ItemSingleUnit = new ItemSingleUnitRepository(_context);
            Company = new CompanyRepository(_context);
        }

        public IVatClassRepository VatClass {get;private set;}
        public IItemRepository Items {get; private set;}
        public IItemPriceRepository ItemPrices {get; private set;}
        public IClientsRepository Clients{get; private set;}
        public IDocumentsRepository Documents {get; private set;}
        public IItemTypesRepository ItemTypes {get; private set;}
        public IDocumentTypesRepository DocumentTypes {get; private set;}
        public IItemPackingRepository ItemPacking {get; private set;}
        public IItemSingleUnitRepository ItemSingleUnit {get; private set;}
        public ICompanyRepository Company {get; private set;}

        public int Complete()
        {
            return _context.SaveChanges();
        }

        public async Task<int> CompleteAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}