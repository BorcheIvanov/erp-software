using ERP.Infrastructure;
using ERP.DomainModel;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.DomainRepository;

namespace ERP.Infrastructure.Repositories
{
    public class DocumentsRepository: Repository<Document>, IDocumentsRepository
    {
        public DocumentsRepository(ApplicationDbContext context) : base(context)
        {
            
        }
        
        public Document GetDocument(int id, int userCompanyID)
        {
            return _context.Documents
                            .Include(doc => doc.Company)
                                .Include(doc => doc.DocumentDetails)
                                    .ThenInclude(docDetails => docDetails.Item)
                            .ThenInclude(item => item.ItemType)
                                .Include(doc => doc.DocumentDetails)
                            .ThenInclude(docDetails => docDetails.VatClass)
                                .Include(doc => doc.DocumentDetails)
                            .ThenInclude(docDetails => docDetails.ItemPrice)
                                .Include(doc => doc.DocumentDetails)
                                    .ThenInclude(docDetails => docDetails.Item)
                            .ThenInclude(item => item.Packing)
                            .Include(doc => doc.Client)
                            .Include(doc => doc.DocumentType)
                            .SingleOrDefault(doc => doc.ID == id && doc.CompanyID == userCompanyID);
        }

        public async Task<IEnumerable<Document>> GetAllDocumentsAsync(int userCompanyID)
        {
            return await _context.Documents
                        .Include(doc => doc.Client)
                        .Include(doc => doc.DocumentType)
                        .Where(doc => doc.CompanyID == userCompanyID)
                        .OrderByDescending(x => x.TimeInsert)
                        .ToListAsync();
        }

        public async Task<IEnumerable<Document>> GetAllDocumentsWithDetailsAsync(int userCompanyID)
        {
            return await _context.Documents
                        .Include(doc => doc.Client)
                        .Include(doc => doc.DocumentType)
                        .Include(doc => doc.DocumentDetails)
                        .Where(doc => doc.CompanyID == userCompanyID)
                        .ToListAsync();
        }

        public decimal GetItemStock(int itemId, int companyId)
        {
        
            //TODO: refactor this
            return _context.Documents
                .Include(doc => doc.Client)
                .Include(doc => doc.DocumentType)
                .Include(doc => doc.DocumentDetails)
                .Where(doc => doc.CompanyID == userCompanyID)
                .Sum(x => x.DocumentDetails.Where(d => d.ItemID == itemId && d.Document.DocumentType.AffectingQuantity)
                    .Sum(xx => xx.Quantity * xx.Document.DocumentType.AffectingValue));
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }

    }
}