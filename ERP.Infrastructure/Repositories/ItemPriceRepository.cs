using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class ItemPriceRepository : Repository<ItemPrice>, IItemPriceRepository
    {
        public ItemPriceRepository(ApplicationDbContext context) : base(context)
        {
        }

        public ApplicationDbContext _context => dbContext as ApplicationDbContext;


        public async Task<IEnumerable<ItemPrice>> GetItemPriceList(int id, int companyId)
        {
            return await _context.ItemPrices.Where(x => x.CompanyID == companyId && x.ItemID == id).OrderBy(x => x.ID)
                .ToListAsync();
        }
    }
}