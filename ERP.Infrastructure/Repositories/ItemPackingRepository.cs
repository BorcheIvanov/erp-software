using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;
using Microsoft.EntityFrameworkCore;

namespace ERP.Infrastructure.Repositories
{
    public class ItemPackingRepository: Repository<ItemPacking>, IItemPackingRepository
    {
        public ItemPackingRepository(ApplicationDbContext contex) : base(contex)
        {
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }

        public async Task<IEnumerable<ItemPacking>> GetItemPackingWithSingleUnit(int userCompanyID)
        {
            return await _context.ItemPacking.Where(x => x.CompanyID == userCompanyID)
                                        .Join(_context.ItemSingleUnit, 
                                                p => p.SingleUnitID, 
                                                u => u.ID, 
                                                (p, u) => new ItemPacking{
                                                    ID = p.ID,
                                                    Name = p.Name,
                                                    SingleUnit = u
                                                }).ToListAsync();
        }
       
    }
}