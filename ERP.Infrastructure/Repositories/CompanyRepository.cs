using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;

namespace ERP.Infrastructure.Repositories
{
    public class CompanyRepository: Repository<Company>, ICompanyRepository
    {
        public CompanyRepository(ApplicationDbContext contex) : base(contex)
        {
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }
    }
}