using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.DomainRepository;

namespace ERP.Infrastructure.Repositories
{
    public class ItemSingleUnitRepository: Repository<ItemSingleUnit>, IItemSingleUnitRepository
    {
        public ItemSingleUnitRepository(ApplicationDbContext contex) : base(contex)
        {
        }

        public ApplicationDbContext _context
        {
            get { return dbContext as ApplicationDbContext; }
        }
    }
}