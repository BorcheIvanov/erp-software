﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ERP.Infrastructure.Migrations
{
    public partial class MoreTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Artikals_ArtikalID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentType_DocumentTypeID",
                table: "Documents");

            migrationBuilder.DropTable(
                name: "Artikals");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentType",
                table: "DocumentType");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_ArtikalID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "ArtikalID",
                table: "DocumentDetails");

            migrationBuilder.RenameTable(
                name: "DocumentType",
                newName: "DocumentTypes");

            migrationBuilder.AddColumn<bool>(
                name: "AffectingQuantity",
                table: "DocumentTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "AffectingValue",
                table: "DocumentTypes",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateTime",
                table: "Documents",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "ItemID",
                table: "DocumentDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentTypes",
                table: "DocumentTypes",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "ItemPrice",
                columns: table => new
                {
                    BasicPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemPrice", x => x.BasicPrice);
                });

            migrationBuilder.CreateTable(
                name: "Items",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Active = table.Column<bool>(type: "bit", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PriceBasicPrice = table.Column<decimal>(type: "decimal(18, 2)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Items", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Items_ItemPrice_PriceBasicPrice",
                        column: x => x.PriceBasicPrice,
                        principalTable: "ItemPrice",
                        principalColumn: "BasicPrice",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_ItemID",
                table: "DocumentDetails",
                column: "ItemID");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PriceBasicPrice",
                table: "Items",
                column: "PriceBasicPrice");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents",
                column: "DocumentTypeID",
                principalTable: "DocumentTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents");

            migrationBuilder.DropTable(
                name: "Items");

            migrationBuilder.DropTable(
                name: "ItemPrice");

            migrationBuilder.DropPrimaryKey(
                name: "PK_DocumentTypes",
                table: "DocumentTypes");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_ItemID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "AffectingQuantity",
                table: "DocumentTypes");

            migrationBuilder.DropColumn(
                name: "AffectingValue",
                table: "DocumentTypes");

            migrationBuilder.DropColumn(
                name: "DateTime",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "ItemID",
                table: "DocumentDetails");

            migrationBuilder.RenameTable(
                name: "DocumentTypes",
                newName: "DocumentType");

            migrationBuilder.AddColumn<int>(
                name: "ArtikalID",
                table: "DocumentDetails",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_DocumentType",
                table: "DocumentType",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "Artikals",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Artikals", x => x.ID);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_ArtikalID",
                table: "DocumentDetails",
                column: "ArtikalID");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Artikals_ArtikalID",
                table: "DocumentDetails",
                column: "ArtikalID",
                principalTable: "Artikals",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentType_DocumentTypeID",
                table: "Documents",
                column: "DocumentTypeID",
                principalTable: "DocumentType",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
