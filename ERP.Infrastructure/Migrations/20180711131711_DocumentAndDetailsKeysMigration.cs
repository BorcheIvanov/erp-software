﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class DocumentAndDetailsKeysMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentTypeID",
                table: "Documents",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClientID",
                table: "Documents",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "DocumentDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "DocumentID",
                table: "DocumentDetails",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ClientID",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "DocumentID1",
                table: "DocumentDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemPriceID",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "VatClassID",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Documents_ClientID",
                table: "Documents",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_ClientID",
                table: "DocumentDetails",
                column: "ClientID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_DocumentID1",
                table: "DocumentDetails",
                column: "DocumentID1");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_ItemPriceID",
                table: "DocumentDetails",
                column: "ItemPriceID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_VatClassID",
                table: "DocumentDetails",
                column: "VatClassID");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Clients_ClientID",
                table: "DocumentDetails",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Documents_DocumentID1",
                table: "DocumentDetails",
                column: "DocumentID1",
                principalTable: "Documents",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_ItemPrices_ItemPriceID",
                table: "DocumentDetails",
                column: "ItemPriceID",
                principalTable: "ItemPrices",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_VatClass_VatClassID",
                table: "DocumentDetails",
                column: "VatClassID",
                principalTable: "VatClass",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Clients_ClientID",
                table: "Documents",
                column: "ClientID",
                principalTable: "Clients",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents",
                column: "DocumentTypeID",
                principalTable: "DocumentTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Clients_ClientID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Documents_DocumentID1",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_ItemPrices_ItemPriceID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_VatClass_VatClassID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Clients_ClientID",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_Documents_ClientID",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_ClientID",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_DocumentID1",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_ItemPriceID",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_VatClassID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "ClientID",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "ClientID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "DocumentID1",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "ItemPriceID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "VatClassID",
                table: "DocumentDetails");

            migrationBuilder.AlterColumn<int>(
                name: "DocumentTypeID",
                table: "Documents",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "DocumentDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "DocumentID",
                table: "DocumentDetails",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Items_ItemID",
                table: "DocumentDetails",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_DocumentTypes_DocumentTypeID",
                table: "Documents",
                column: "DocumentTypeID",
                principalTable: "DocumentTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
