﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class PriceID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPrice_PriceBasicPrice",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_PriceBasicPrice",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice");

            migrationBuilder.DropColumn(
                name: "PriceBasicPrice",
                table: "Items");

            migrationBuilder.AddColumn<int>(
                name: "PriceID",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ID",
                table: "ItemPrice",
                type: "int",
                nullable: false,
                defaultValue: 0)
                .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "DocumentTypes",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice",
                column: "ID");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PriceID",
                table: "Items",
                column: "PriceID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPrice_PriceID",
                table: "Items",
                column: "PriceID",
                principalTable: "ItemPrice",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPrice_PriceID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_PriceID",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice");

            migrationBuilder.DropColumn(
                name: "PriceID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ID",
                table: "ItemPrice");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "DocumentTypes");

            migrationBuilder.AddColumn<decimal>(
                name: "PriceBasicPrice",
                table: "Items",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice",
                column: "BasicPrice");

            migrationBuilder.CreateIndex(
                name: "IX_Items_PriceBasicPrice",
                table: "Items",
                column: "PriceBasicPrice");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPrice_PriceBasicPrice",
                table: "Items",
                column: "PriceBasicPrice",
                principalTable: "ItemPrice",
                principalColumn: "BasicPrice",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
