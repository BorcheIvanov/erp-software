﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class test5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "PackingID",
                table: "Items",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemTypeID",
                table: "Items",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "ItemPrices",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items",
                column: "ItemTypeID",
                principalTable: "ItemTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items",
                column: "PackingID",
                principalTable: "ItemPacking",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "PackingID",
                table: "Items",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ItemTypeID",
                table: "Items",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AlterColumn<int>(
                name: "ItemID",
                table: "ItemPrices",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items",
                column: "ItemTypeID",
                principalTable: "ItemTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items",
                column: "PackingID",
                principalTable: "ItemPacking",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
