﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ERP.Infrastructure.Migrations
{
    public partial class ItemPriceUpdatedMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BasicPrice",
                table: "ItemPrices");

            migrationBuilder.AddColumn<decimal>(
                name: "Value",
                table: "VatClass",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateValidFrom",
                table: "ItemPrices",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "DateValidTo",
                table: "ItemPrices",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "ItemPrices",
                type: "decimal(18, 2)",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "PriceWithVAT",
                table: "ItemPrices",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Value",
                table: "VatClass");

            migrationBuilder.DropColumn(
                name: "DateValidFrom",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "DateValidTo",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "PriceWithVAT",
                table: "ItemPrices");

            migrationBuilder.AddColumn<decimal>(
                name: "BasicPrice",
                table: "ItemPrices",
                nullable: false,
                defaultValue: 0m);
        }
    }
}
