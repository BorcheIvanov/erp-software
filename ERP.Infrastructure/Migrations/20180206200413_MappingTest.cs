﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class MappingTest : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "CompanyID",
                table: "Items",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items");

            migrationBuilder.AlterColumn<int>(
                name: "CompanyID",
                table: "Items",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
