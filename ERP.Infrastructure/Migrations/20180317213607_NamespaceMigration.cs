﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class NamespaceMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemPacking_ItemSingleUnit_SingleUnitID",
                table: "ItemPacking");

            migrationBuilder.AlterColumn<int>(
                name: "SingleUnitID",
                table: "ItemPacking",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPacking_ItemSingleUnit_SingleUnitID",
                table: "ItemPacking",
                column: "SingleUnitID",
                principalTable: "ItemSingleUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemPacking_ItemSingleUnit_SingleUnitID",
                table: "ItemPacking");

            migrationBuilder.AlterColumn<int>(
                name: "SingleUnitID",
                table: "ItemPacking",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPacking_ItemSingleUnit_SingleUnitID",
                table: "ItemPacking",
                column: "SingleUnitID",
                principalTable: "ItemSingleUnit",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
