﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class DetailsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Inventory_InventoryID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Invoice_InvoiceID",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_InventoryID",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_InvoiceID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "InventoryID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "InvoiceID",
                table: "DocumentDetails");

            migrationBuilder.CreateTable(
                name: "InventoryDetails",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InventoryID = table.Column<int>(type: "int", nullable: true),
                    ItemID = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InventoryDetails_Inventory_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InventoryDetails_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceDetails",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InvoiceID = table.Column<int>(type: "int", nullable: true),
                    ItemID = table.Column<int>(type: "int", nullable: true),
                    Quantity = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InvoiceDetails_Invoice_InvoiceID",
                        column: x => x.InvoiceID,
                        principalTable: "Invoice",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoiceDetails_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_InventoryDetails_InventoryID",
                table: "InventoryDetails",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryDetails_ItemID",
                table: "InventoryDetails",
                column: "ItemID");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetails_InvoiceID",
                table: "InvoiceDetails",
                column: "InvoiceID");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetails_ItemID",
                table: "InvoiceDetails",
                column: "ItemID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryDetails");

            migrationBuilder.DropTable(
                name: "InvoiceDetails");

            migrationBuilder.AddColumn<int>(
                name: "InventoryID",
                table: "DocumentDetails",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "InvoiceID",
                table: "DocumentDetails",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_InventoryID",
                table: "DocumentDetails",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_InvoiceID",
                table: "DocumentDetails",
                column: "InvoiceID");

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Inventory_InventoryID",
                table: "DocumentDetails",
                column: "InventoryID",
                principalTable: "Inventory",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Invoice_InvoiceID",
                table: "DocumentDetails",
                column: "InvoiceID",
                principalTable: "Invoice",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
