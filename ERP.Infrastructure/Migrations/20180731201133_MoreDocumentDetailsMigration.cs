﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class MoreDocumentDetailsMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "VatClass",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ItemSingleUnit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "ItemPacking",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<decimal>(
                name: "BasicPrice",
                table: "Documents",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Documents",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "VatPrice",
                table: "Documents",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalBasicPrice",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalPrice",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<decimal>(
                name: "TotalVatPrice",
                table: "DocumentDetails",
                nullable: false,
                defaultValue: 0m);

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Clients",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "VatClass");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ItemSingleUnit");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "ItemPacking");

            migrationBuilder.DropColumn(
                name: "BasicPrice",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "Price",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "VatPrice",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "TotalBasicPrice",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "TotalPrice",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "TotalVatPrice",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "Active",
                table: "Clients");
        }
    }
}
