﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class ItemPrices : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPrices_PriceID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_PriceID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "PriceID",
                table: "Items");

            migrationBuilder.AddColumn<int>(
                name: "ItemTypeID",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ItemID",
                table: "ItemPrices",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_ItemTypeID",
                table: "Items",
                column: "ItemTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemPrices_ItemID",
                table: "ItemPrices",
                column: "ItemID");

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices",
                column: "ItemID",
                principalTable: "Items",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items",
                column: "ItemTypeID",
                principalTable: "ItemTypes",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ItemPrices_Items_ItemID",
                table: "ItemPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemTypes_ItemTypeID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_Items_ItemTypeID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_ItemPrices_ItemID",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "ItemTypeID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "ItemID",
                table: "ItemPrices");

            migrationBuilder.AddColumn<int>(
                name: "PriceID",
                table: "Items",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Items_PriceID",
                table: "Items",
                column: "PriceID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPrices_PriceID",
                table: "Items",
                column: "PriceID",
                principalTable: "ItemPrices",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
