﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ERP.Infrastructure.Migrations
{
    public partial class CompanyID : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InventoryDetails");

            migrationBuilder.DropTable(
                name: "InvoiceDetails");

            migrationBuilder.DropTable(
                name: "Inventory");

            migrationBuilder.DropTable(
                name: "Invoice");

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "ItemTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "ItemSingleUnit",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "ItemPrices",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "ItemPacking",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "DocumentTypes",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "Documents",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "DocumentDetails",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "Clients",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "CompanyID",
                table: "AspNetUsers",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ItemTypes_CompanyID",
                table: "ItemTypes",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemSingleUnit_CompanyID",
                table: "ItemSingleUnit",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_Items_CompanyID",
                table: "Items",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemPrices_CompanyID",
                table: "ItemPrices",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemPacking_CompanyID",
                table: "ItemPacking",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentTypes_CompanyID",
                table: "DocumentTypes",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_Documents_CompanyID",
                table: "Documents",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_DocumentDetails_CompanyID",
                table: "DocumentDetails",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_Clients_CompanyID",
                table: "Clients",
                column: "CompanyID");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_CompanyID",
                table: "AspNetUsers",
                column: "CompanyID");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_Company_CompanyID",
                table: "AspNetUsers",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Clients_Company_CompanyID",
                table: "Clients",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_DocumentTypes_Company_CompanyID",
                table: "DocumentTypes",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemPrices_Company_CompanyID",
                table: "ItemPrices",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemSingleUnit_Company_CompanyID",
                table: "ItemSingleUnit",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ItemTypes_Company_CompanyID",
                table: "ItemTypes",
                column: "CompanyID",
                principalTable: "Company",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_Company_CompanyID",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_Clients_Company_CompanyID",
                table: "Clients");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentDetails_Company_CompanyID",
                table: "DocumentDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_Documents_Company_CompanyID",
                table: "Documents");

            migrationBuilder.DropForeignKey(
                name: "FK_DocumentTypes_Company_CompanyID",
                table: "DocumentTypes");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemPacking_Company_CompanyID",
                table: "ItemPacking");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemPrices_Company_CompanyID",
                table: "ItemPrices");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_Company_CompanyID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemSingleUnit_Company_CompanyID",
                table: "ItemSingleUnit");

            migrationBuilder.DropForeignKey(
                name: "FK_ItemTypes_Company_CompanyID",
                table: "ItemTypes");

            migrationBuilder.DropIndex(
                name: "IX_ItemTypes_CompanyID",
                table: "ItemTypes");

            migrationBuilder.DropIndex(
                name: "IX_ItemSingleUnit_CompanyID",
                table: "ItemSingleUnit");

            migrationBuilder.DropIndex(
                name: "IX_Items_CompanyID",
                table: "Items");

            migrationBuilder.DropIndex(
                name: "IX_ItemPrices_CompanyID",
                table: "ItemPrices");

            migrationBuilder.DropIndex(
                name: "IX_ItemPacking_CompanyID",
                table: "ItemPacking");

            migrationBuilder.DropIndex(
                name: "IX_DocumentTypes_CompanyID",
                table: "DocumentTypes");

            migrationBuilder.DropIndex(
                name: "IX_Documents_CompanyID",
                table: "Documents");

            migrationBuilder.DropIndex(
                name: "IX_DocumentDetails_CompanyID",
                table: "DocumentDetails");

            migrationBuilder.DropIndex(
                name: "IX_Clients_CompanyID",
                table: "Clients");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_CompanyID",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "ItemTypes");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "ItemSingleUnit");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "ItemPacking");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "DocumentTypes");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "DocumentDetails");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "Clients");

            migrationBuilder.DropColumn(
                name: "CompanyID",
                table: "AspNetUsers");

            migrationBuilder.CreateTable(
                name: "Inventory",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(nullable: false),
                    DocumentTypeID = table.Column<int>(nullable: true),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Inventory", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Inventory_DocumentTypes_DocumentTypeID",
                        column: x => x.DocumentTypeID,
                        principalTable: "DocumentTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Invoice",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DateTime = table.Column<DateTime>(nullable: false),
                    DocumentTypeID = table.Column<int>(nullable: true),
                    Year = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Invoice", x => x.ID);
                    table.ForeignKey(
                        name: "FK_Invoice_DocumentTypes_DocumentTypeID",
                        column: x => x.DocumentTypeID,
                        principalTable: "DocumentTypes",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InventoryDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InventoryID = table.Column<int>(nullable: true),
                    ItemID = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InventoryDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InventoryDetails_Inventory_InventoryID",
                        column: x => x.InventoryID,
                        principalTable: "Inventory",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InventoryDetails_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "InvoiceDetails",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    InvoiceID = table.Column<int>(nullable: true),
                    ItemID = table.Column<int>(nullable: true),
                    Quantity = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InvoiceDetails", x => x.ID);
                    table.ForeignKey(
                        name: "FK_InvoiceDetails_Invoice_InvoiceID",
                        column: x => x.InvoiceID,
                        principalTable: "Invoice",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_InvoiceDetails_Items_ItemID",
                        column: x => x.ItemID,
                        principalTable: "Items",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Inventory_DocumentTypeID",
                table: "Inventory",
                column: "DocumentTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryDetails_InventoryID",
                table: "InventoryDetails",
                column: "InventoryID");

            migrationBuilder.CreateIndex(
                name: "IX_InventoryDetails_ItemID",
                table: "InventoryDetails",
                column: "ItemID");

            migrationBuilder.CreateIndex(
                name: "IX_Invoice_DocumentTypeID",
                table: "Invoice",
                column: "DocumentTypeID");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetails_InvoiceID",
                table: "InvoiceDetails",
                column: "InvoiceID");

            migrationBuilder.CreateIndex(
                name: "IX_InvoiceDetails_ItemID",
                table: "InvoiceDetails",
                column: "ItemID");
        }
    }
}
