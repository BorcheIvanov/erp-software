﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ERP.Infrastructure.Migrations
{
    public partial class VatClassMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Code",
                table: "Items",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "VatClassID",
                table: "Items",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "VatClass",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CompanyID = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VatClass", x => x.ID);
                    table.ForeignKey(
                        name: "FK_VatClass_Company_CompanyID",
                        column: x => x.CompanyID,
                        principalTable: "Company",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_VatClassID",
                table: "Items",
                column: "VatClassID");

            migrationBuilder.CreateIndex(
                name: "IX_VatClass_CompanyID",
                table: "VatClass",
                column: "CompanyID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_VatClass_VatClassID",
                table: "Items",
                column: "VatClassID",
                principalTable: "VatClass",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_VatClass_VatClassID",
                table: "Items");

            migrationBuilder.DropTable(
                name: "VatClass");

            migrationBuilder.DropIndex(
                name: "IX_Items_VatClassID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "Code",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "VatClassID",
                table: "Items");
        }
    }
}
