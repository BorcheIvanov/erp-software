﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

namespace ERP.Infrastructure.Migrations
{
    public partial class NewItemViewModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPrice_PriceID",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice");

            migrationBuilder.DropColumn(
                name: "DateTime",
                table: "Documents");

            migrationBuilder.RenameTable(
                name: "ItemPrice",
                newName: "ItemPrices");

            migrationBuilder.AddColumn<int>(
                name: "PackingID",
                table: "Items",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DocumentDate",
                table: "Documents",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<int>(
                name: "Reverese_Year",
                table: "Documents",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Reverse_DocumentType_ID",
                table: "Documents",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Reverse_ID",
                table: "Documents",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "TimeInsert",
                table: "Documents",
                type: "datetime2",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<decimal>(
                name: "Quantity",
                table: "DocumentDetails",
                type: "decimal(18, 2)",
                nullable: false,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemPrices",
                table: "ItemPrices",
                column: "ID");

            migrationBuilder.CreateTable(
                name: "ItemSingleUnit",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemSingleUnit", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "ItemPacking",
                columns: table => new
                {
                    ID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SingleUnitID = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ItemPacking", x => x.ID);
                    table.ForeignKey(
                        name: "FK_ItemPacking_ItemSingleUnit_SingleUnitID",
                        column: x => x.SingleUnitID,
                        principalTable: "ItemSingleUnit",
                        principalColumn: "ID",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Items_PackingID",
                table: "Items",
                column: "PackingID");

            migrationBuilder.CreateIndex(
                name: "IX_ItemPacking_SingleUnitID",
                table: "ItemPacking",
                column: "SingleUnitID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items",
                column: "PackingID",
                principalTable: "ItemPacking",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPrices_PriceID",
                table: "Items",
                column: "PriceID",
                principalTable: "ItemPrices",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPacking_PackingID",
                table: "Items");

            migrationBuilder.DropForeignKey(
                name: "FK_Items_ItemPrices_PriceID",
                table: "Items");

            migrationBuilder.DropTable(
                name: "ItemPacking");

            migrationBuilder.DropTable(
                name: "ItemSingleUnit");

            migrationBuilder.DropIndex(
                name: "IX_Items_PackingID",
                table: "Items");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ItemPrices",
                table: "ItemPrices");

            migrationBuilder.DropColumn(
                name: "PackingID",
                table: "Items");

            migrationBuilder.DropColumn(
                name: "DocumentDate",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "Reverese_Year",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "Reverse_DocumentType_ID",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "Reverse_ID",
                table: "Documents");

            migrationBuilder.DropColumn(
                name: "TimeInsert",
                table: "Documents");

            migrationBuilder.RenameTable(
                name: "ItemPrices",
                newName: "ItemPrice");

            migrationBuilder.AddColumn<DateTime>(
                name: "DateTime",
                table: "Documents",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AlterColumn<int>(
                name: "Quantity",
                table: "DocumentDetails",
                nullable: false,
                oldClrType: typeof(decimal),
                oldType: "decimal(18, 2)");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ItemPrice",
                table: "ItemPrice",
                column: "ID");

            migrationBuilder.AddForeignKey(
                name: "FK_Items_ItemPrice_PriceID",
                table: "Items",
                column: "PriceID",
                principalTable: "ItemPrice",
                principalColumn: "ID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
