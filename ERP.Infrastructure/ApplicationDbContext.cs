﻿//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using ERP.DomainModel;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace ERP.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }

        public DbSet<Item> Items{get;set;}
        public DbSet<Document> Documents{get;set;}
        public DbSet<DocumentDetails> DocumentDetails {get;set;}
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Company> Company { get; set; }
        public DbSet<ItemPrice> ItemPrices { get; set; }
        public DbSet<ItemType> ItemTypes {get;set;}
        public DbSet<ItemPacking> ItemPacking { get; set; }
        public DbSet<ItemSingleUnit> ItemSingleUnit { get; set; }
        public DbSet<VatClass> VatClass { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            builder.Entity("ERP.DomainModel.Item", b =>
                {
                    b.HasOne("ERP.DomainModel.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.VatClass", "VatClass")
                        .WithMany()
                        .HasForeignKey("VatClassID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.ItemType", "ItemType")
                        .WithMany()
                        .HasForeignKey("ItemTypeID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.ItemPacking", "Packing")
                        .WithMany()
                        .HasForeignKey("PackingID")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            builder.Entity("ERP.DomainModel.ItemPacking", b =>
                {
                    b.HasOne("ERP.DomainModel.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.ItemSingleUnit", "SingleUnit")
                        .WithMany()
                        .HasForeignKey("SingleUnitID")
                        .OnDelete(DeleteBehavior.Restrict);
                });
            
            builder.Entity("ERP.DomainModel.DocumentDetails", b => 
                {
                    b.HasOne("ERP.DomainModel.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.VatClass", "VatClass")
                        .WithMany()
                        .HasForeignKey("VatClassID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.ItemPrice", "ItemPrice")
                        .WithMany()
                        .HasForeignKey("ItemPriceID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.ItemPrice", "ItemPrice")
                        .WithMany()
                        .HasForeignKey("ItemPriceID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.Document", "Document")
                        .WithMany()
                        .HasForeignKey("DocumentID")
                        .OnDelete(DeleteBehavior.Restrict);
                });

            builder.Entity("ERP.DomainModel.Document", b =>
                {
                    b.HasOne("ERP.DomainModel.Company", "Company")
                        .WithMany()
                        .HasForeignKey("CompanyID")
                        .OnDelete(DeleteBehavior.Restrict);
                    b.HasOne("ERP.DomainModel.Client", "Client")
                        .WithMany()
                        .HasForeignKey("ClientID")
                        .OnDelete(DeleteBehavior.Restrict);
                });
        }
    }
}
