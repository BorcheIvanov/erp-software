using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ERP.DomainModel
{
    public class Item
    {
        [Key] public int ID { get; set; }

        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; }
        public List<ItemPrice> Price { get; set; }

        [NotMapped] public ItemPrice ItemPrice { get; set; }

        public int PackingID { get; set; }
        public ItemPacking Packing { get; set; }
        public int ItemTypeID { get; set; }
        public ItemType ItemType { get; set; }
        public int VatClassID { get; set; }
        public VatClass VatClass { get; set; }
        public int CompanyID { get; set; }
        public Company Company { get; set; }

        [NotMapped] public decimal Stock { get; set; }
    }
}