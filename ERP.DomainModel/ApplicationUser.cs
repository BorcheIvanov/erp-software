﻿
using Microsoft.AspNetCore.Identity;

namespace ERP.DomainModel
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {

        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}
