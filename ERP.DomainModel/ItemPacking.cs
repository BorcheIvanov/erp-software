using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class ItemPacking
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int SingleUnitID { get; set; }
        public ItemSingleUnit SingleUnit { get; set; }
        public bool Active { get; set; }
        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}