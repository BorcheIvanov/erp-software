using System;
using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class ItemPrice
    {
        [Key]
        public int ID { get; set; }
        public decimal Price { get; set; }
        public bool PriceWithVAT { get; set; }
        public DateTime DateValidFrom { get; set; }
        public DateTime DateValidTo { get; set; }
        public int CompanyID { get; set; }
        public int ItemID { get; set; }
        //May be deleted
        public virtual Item Item { get; set; }
        public virtual Company Company { get; set; }
        
    }
}