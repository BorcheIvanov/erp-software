using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class Client
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }
        public int DaysForPaying { get; set; }
        public string UniqueNumber { get; set; }
        public string TaxNumber { get; set; }
        public string Address {get;set;}
        public string PhoneNumber { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public bool Active { get; set; }
        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}