
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class Document
    {
        [Key]
        public int ID { get; set; }
        public int Year { get; set; } = DateTime.Now.Year;
        public int ClientID { get; set; }
        public Client Client { get; set; }
        public int DocumentTypeID { get; set; }
        public DocumentType DocumentType { get; set; }
        public DateTime DocumentDate { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal VatPrice { get; set; }
        public decimal Price { get; set; }
        public DateTime TimeInsert { get; set; } = DateTime.Now;
        public int Reverse_ID { get; set; }
        public int Reverese_Year { get; set; }
        public int Reverse_DocumentType_ID { get; set; }
        public List<DocumentDetails> DocumentDetails {get;set;} 
        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}