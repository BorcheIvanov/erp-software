using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class ItemType
    {
        [Key]
        public int ID { get; set; }
        public string Name { get; set; }

        public bool Active { get; set; }
        public bool AffectingQuantity { get; set; }
        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}