
using System.ComponentModel.DataAnnotations;

namespace ERP.DomainModel
{
    public class DocumentDetails
    {
        [Key]
        public int ID { get; set; }     
        public int DocumentID { get; set; }
        public Document Document { get; set; }
        public int ItemID { get; set; }
        public Item Item {get;set;}
        public int ItemPriceID { get; set; }
        public ItemPrice ItemPrice { get; set; }
        public int VatClassID { get; set; }
        public VatClass VatClass { get; set; }
        public decimal Quantity { get; set; }
        public decimal TotalBasicPrice { get; set; }
        public decimal TotalVatPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public int ClientID { get; set; }
        public Client Client { get; set; }
        public int CompanyID { get; set; }
        public Company Company { get; set; }
    }
}