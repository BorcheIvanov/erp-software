using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace ERP.DomainRepository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        TEntity GetByID(int id, int userCompanyID);
        Task<TEntity> GetByIDAsync(int id, int userCompanyID);
        IEnumerable<TEntity> GetAll(int userCompanyID);

        Task<IEnumerable<TEntity>> GetAllAsync(int userCompanyID);

        TEntity FindSingle(Expression<Func<TEntity, bool>> predicate);
        Task<TEntity> FindSingleAsync(Expression<Func<TEntity, bool>> predicate);
        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        void Add(TEntity entity);
        void AddOrUpdate(TEntity entity);
        void AddRange(IEnumerable<TEntity> entities);

        void Remove(TEntity entity);
        void RemoveRange(IEnumerable<TEntity> entities);
    }
}