using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IItemSingleUnitRepository : IRepository<ItemSingleUnit>
    {
        
    }
}