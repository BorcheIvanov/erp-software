using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IItemTypesRepository : IRepository<ItemType>
    {
        Task<IEnumerable<ItemType>> GetActiveItemTypes(int userCompanyID);
    }
}