using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IVatClassRepository : IRepository<VatClass>
    {
        Task<IEnumerable<VatClass>> GetActiveVatClass(int userCompanyID);
    }
}