using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface ICompanyRepository : IRepository<Company>
    {
        
    }
}