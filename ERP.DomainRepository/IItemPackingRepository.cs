using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IItemPackingRepository : IRepository<ItemPacking>
    {
        Task<IEnumerable<ItemPacking>> GetItemPackingWithSingleUnit(int userCompanyID);
    }
}