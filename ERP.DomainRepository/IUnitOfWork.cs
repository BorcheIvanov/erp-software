using System;
using System.Threading.Tasks;

namespace ERP.DomainRepository
{
    public interface IUnitOfWork : IDisposable
    {
        IVatClassRepository VatClass {get;}
        IItemRepository Items {get;}
        IItemPriceRepository ItemPrices { get; }
        IClientsRepository Clients {get;}
        IDocumentsRepository Documents {get;}

        IItemTypesRepository ItemTypes{get;}
        IDocumentTypesRepository DocumentTypes{get;}
        IItemPackingRepository ItemPacking{get;}
        IItemSingleUnitRepository ItemSingleUnit{get;}
        ICompanyRepository Company {get;}

        int Complete();
        Task<int> CompleteAsync();
    }
}