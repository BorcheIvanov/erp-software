using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IClientsRepository:IRepository<Client>
    {
        Task<IEnumerable<Client>> Search(int userCompanyID, string text); 
    }
}