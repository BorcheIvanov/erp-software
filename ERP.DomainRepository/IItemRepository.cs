using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IItemRepository : IRepository<Item>
    {
        Task<Item> GetItemWithPrice(int itemID, int userCompanyID);
        Task<IEnumerable<Item>> GetItemListWithPrices(int userCompanyID);
        Task<IEnumerable<Item>> Search(int userCompanyID, string text);
        Task<IEnumerable<Item>> GetItemsStock(int companyId);
    }
}