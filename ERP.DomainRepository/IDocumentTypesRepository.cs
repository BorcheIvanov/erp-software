using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IDocumentTypesRepository : IRepository<DocumentType>
    {
        Task<IEnumerable<DocumentType>> GetActiveDocumentTypes(int userCompanyID);
    }
}