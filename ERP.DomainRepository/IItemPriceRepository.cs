using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IItemPriceRepository : IRepository<ItemPrice>
    {
        Task<IEnumerable<ItemPrice>> GetItemPriceList(int id, int companyID);
    }
}