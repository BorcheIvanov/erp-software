using System.Collections.Generic;
using System.Threading.Tasks;
using ERP.DomainModel;

namespace ERP.DomainRepository
{
    public interface IDocumentsRepository : IRepository<Document>
    {
        Document GetDocument(int id, int userCompanyID);
        Task<IEnumerable<Document>> GetAllDocumentsAsync(int userCompanyID);
        Task<IEnumerable<Document>> GetAllDocumentsWithDetailsAsync(int userCompanyID);
        decimal GetItemStock(int itemId, int companyId);
    }
    
}