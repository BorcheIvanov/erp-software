using System;
using AutoMapper;
using ERP.DomainModel;
using ERP.ViewModels;
using ERP.ViewModels.ClientsViewModels;
using ERP.ViewModels.DocumentsViewModels;
using ERP.ViewModels.ItemsViewModels;

namespace ERP.Mapper
{
    public class DomainProfile : Profile
    {
        public DomainProfile()
        {
            CreateMap<Client, ClientViewModel>();
            CreateMap<ClientViewModel, Client>();

            CreateMap<ItemType, ItemTypeViewModel>()
                .ForMember(x => x.IsNotNew, opt => opt.MapFrom(o => true));
            CreateMap<ItemTypeViewModel, ItemType>();
            CreateMap<ItemType, KeyValue>()
                .ForMember(x => x.Key, opt => opt.MapFrom(o => o.ID))
                .ForMember(x => x.Value, opt => opt.MapFrom(o => o.Name));

            CreateMap<ItemPacking, KeyValue>()
                .ForMember(x => x.Key, opt => opt.MapFrom(o => o.ID))
                .ForMember(x => x.Value, opt => opt.MapFrom(o => o.Name));

            CreateMap<VatClass, VatClassViewModel>()
                .ForMember(x => x.IsNotNew, opt => opt.MapFrom(o => true));
            CreateMap<VatClassViewModel, VatClass>();
            CreateMap<VatClass, KeyValue>()
                .ForMember(x => x.Key, opt => opt.MapFrom(o => o.ID))
                .ForMember(x => x.Value, opt => opt.MapFrom(o => $"{o.Name} - {o.Value}%"));

            CreateMap<Item, ItemViewModel>()
                .ForMember(x => x.ItemType, opt => opt.MapFrom(o => o.ItemType.Name))
                .ForMember(x => x.VatClassName, opt => opt.MapFrom(o => $"{o.VatClass.Name} - {o.VatClass.Value}%"))
                .ForMember(x => x.ItemPackageID, opt => opt.MapFrom(o => o.PackingID))
                .ForMember(x => x.DateValidFrom, opt => opt.MapFrom(o => o.ItemPrice.DateValidFrom))
                .ForMember(x => x.DateValidTo, opt => opt.MapFrom(o => o.ItemPrice.DateValidTo))
                .ForMember(x => x.Price, opt => opt.MapFrom(o => o.ItemPrice.Price))
                .ForMember(x => x.PriceWithVAT, opt => opt.MapFrom(o => o.ItemPrice.PriceWithVAT))
                .ForMember(x => x.IsNotNew, opt => opt.MapFrom(o => true));

            CreateMap<ItemPacking, ItemPackingViewModel>()
                .ForMember(x => x.ItemSingleUnitName, opt => opt.MapFrom(o => o.SingleUnit.Name));
            CreateMap<ItemPackingViewModel, ItemPacking>();

            CreateMap<ItemSingleUnit, KeyValue>()
                .ForMember(x => x.Key, opt => opt.MapFrom(o => o.ID))
                .ForMember(x => x.Value, opt => opt.MapFrom(o => o.Name));

            CreateMap<Item, ItemPriceViewModel>()
                .ForMember(x => x.ID, opt => opt.MapFrom(o => o.ItemPrice.ID))
                .ForMember(x => x.ItemID, opt => opt.MapFrom(o => o.ID))
                .ForMember(x => x.ItemName, opt => opt.MapFrom(i => $"{i.Code} {i.Name} ({i.Description})"))
                .ForMember(x => x.Price, opt => opt.MapFrom(o => o.ItemPrice.Price))
                .ForMember(x => x.DateValidFrom, opt => opt.MapFrom(o => o.ItemPrice.DateValidFrom))
                .ForMember(x => x.DateValidFrom, opt => opt.MapFrom(o => DateTime.Now))
                .ForMember(x => x.DateValidTo, opt => opt.MapFrom(o => DateTime.Now.AddYears(1)))
                .ForMember(x => x.ItemsList, opt => opt.Ignore())
                .ForMember(x => x.PriceList, opt => opt.Ignore());

            CreateMap<ItemPrice, ItemPriceViewModel>()
                .ForMember(x => x.ItemName, opt => opt.MapFrom(i => $"{i.Item.Code} {i.Item.Name} ({i.Item.Description})"))
                .ForMember(x => x.ItemsList, opt => opt.Ignore())
                .ForMember(x => x.PriceList, opt => opt.Ignore());
            
            CreateMap<ItemPriceViewModel, ItemPrice>();

            CreateMap<Item, ItemStockViewModel>();

            CreateMap<DocumentType, DocumentTypeViewModel>()
                .ForMember(x => x.IsNotNew, opt => opt.MapFrom(o => true));
            CreateMap<DocumentTypeViewModel, DocumentType>();

            CreateMap<DocumentViewModel, Document>();
            CreateMap<Document, DocumentViewModel>()
                .ForMember(x => x.DocumentNumber, opt => opt.MapFrom(d => $"{d.ID}-{d.DocumentTypeID}/{d.Year}"))
                .ForMember(x=>x.DateForPayingDue, opt => opt.MapFrom(d => d.DocumentDate.AddDays(d.Client.DaysForPaying)))
                .ForMember(x => x.Date, opt => opt.MapFrom(d => d.DocumentDate));

            CreateMap<Item, ItemStockViewModel>();
        }

        
    }
}