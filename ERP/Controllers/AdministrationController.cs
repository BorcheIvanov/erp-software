using System;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using ERP.Infrastructure;
using ERP.DomainModel;
using ERP.Extensions;
using ERP.ViewModels.AdministrationViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace ERP.Controllers
{
    [Authorize]
    public class AdministrationController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public AdministrationController(UserManager<ApplicationUser> userManager, ApplicationDbContext context)
        {
            _userManager = userManager;
            _context = context;
        }

        public async Task<IActionResult> Index(string id)
        {
            ViewData["Title"] = "Administration";
            var userCompanyId = User.Identity.GetCompanyID();
            var roles = await _context.Roles.Select(r => new {Key = r.Name, Value = r.Name})
                .ToListAsync();

            var model = new IndexViewModel();
            var company = await _context.Company.Where(x => x.ID == userCompanyId).FirstOrDefaultAsync();
            if (company != null)
            {
                model.ID = company.ID;
                model.Name = company.Name;
                model.UniqueNumber = company.UniqueNumber;
                model.TaxNumber = company.TaxNumber;
                model.Address = company.Address;
                model.PhoneNumber = company.PhoneNumber;
                model.ZipCode = company.ZipCode;
                model.City = company.City;
                model.Country = company.Country;
                if (company.Logo != null && company.Logo.Length > 0)
                    model.LogoSrc = $"data:image/jpg;base64,{Convert.ToBase64String(company.Logo)}";
            }

            model.User = new UsersViewModel
            {
                List = await (from u in _context.Users
                    join ur in _context.UserRoles on u.Id equals ur.UserId
                    join r in _context.Roles on ur.RoleId equals r.Id
                    where u.CompanyID == userCompanyId
                    select new UsersViewModel
                    {
                        ID = u.Id,
                        UserName = u.UserName,
                        Role = r.Name,
                        Active = true
                    }).ToListAsync(),
                RolesList = new SelectList(roles, "Key", "Value")
            };

            if (!string.IsNullOrEmpty(id))
            {
                model.User.ID = id;
                model.User.UserName = model.User.List.Where(x => x.ID == id)
                    .Select(u => u.UserName)
                    .FirstOrDefault();
            }

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> UpdateCompany(IndexViewModel model)
        {
            if (ModelState.IsValid)
            {
                var c = new Company
                {
                    ID = model.ID,
                    Name = model.Name,
                    UniqueNumber = model.UniqueNumber,
                    TaxNumber = model.TaxNumber,
                    Address = model.Address,
                    PhoneNumber = model.PhoneNumber,
                    Country = model.Country,
                    City = model.City,
                    ZipCode = model.ZipCode
                };

                if (model.LogoFile != null && model.LogoFile.Length > 0)
                {
                    using (var stream = new MemoryStream())
                    {
                        model.LogoFile.CopyTo(stream);
                        model.Logo = stream.ToArray();
                    }

                    c.Logo = model.Logo;
                }
                else
                    c.Logo = await _context.Company.Where(x => x.ID == model.ID)
                        .Select(x => x.Logo)
                        .FirstOrDefaultAsync();

                _context.Company.Update(c);
                await _context.SaveChangesAsync();
            }

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Users(IndexViewModel model)
        {
            if (!string.IsNullOrEmpty(model.User.UserName) && !string.IsNullOrEmpty(model.User.Role))
            {
                var userCompanyId = User.Identity.GetCompanyID();
                var user = new ApplicationUser
                {
                    UserName = model.User.UserName,
                    Company = await _context.Company.Where(c => c.ID == userCompanyId).FirstOrDefaultAsync()
                };
                var result = await _userManager.CreateAsync(user, "Sparkasse_1");
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, model.User.Role);
                    await _userManager.AddClaimAsync(user, new Claim("CompanyID", user.CompanyID.ToString()));
                }
                
            }

            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Roles()
        {
            var roles = await _context.Roles.ToListAsync();
            var model = new RolesViewModel();
            foreach (var role in roles)
                model.Roles.Add(new RoleViewModel
                {
                    Role = role.Name
                });
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Roles(RolesViewModel model)
        {
            if (ModelState.IsValid)
                if (!string.IsNullOrEmpty(model.Role))
                {
                    _context.Roles.Add(new IdentityRole
                    {
                        Name = model.Role,
                        NormalizedName = model.Role.ToUpper()
                    });
                    await _context.SaveChangesAsync();
                }

            return RedirectToAction("Roles");
        }
    }
}