using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels.ClientsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers
{
    [Authorize]
    public class ClientsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ClientsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index(int? id)
        {
            ViewData["Title"] = "Clients";

            var model = new ClientViewModel
            {
                Active = true
            };

            if (id != null)
            {
                var client =
                    _mapper.Map<ClientViewModel>(
                        await _unitOfWork.Clients.GetByIDAsync((int) id, User.Identity.GetCompanyID()));
                client.IsNotNew = true;
                model = client;
            }

            model.ClientList =
                _mapper.Map<List<ClientViewModel>>(await _unitOfWork.Clients.GetAllAsync(User.Identity.GetCompanyID()));

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Index(ClientViewModel model)
        {
            if (ModelState.IsValid)
            {
                var client = _mapper.Map<Client>(model);
                client.CompanyID = User.Identity.GetCompanyID();

                _unitOfWork.Clients.AddOrUpdate(client);
                await _unitOfWork.CompleteAsync();

                model.IsNotNew = true;
                model.ClientList =
                    _mapper.Map<List<ClientViewModel>>(
                        await _unitOfWork.Clients.GetAllAsync(User.Identity.GetCompanyID()));
            }

            return View(model);
        }
    }
}