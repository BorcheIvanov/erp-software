using System.Threading.Tasks;
using AutoMapper;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels.ClientsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Api
{
    [Authorize]
    [Route("api/Clients/[Action]")]
    [ApiController]
    public class ClientsApiController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ClientsApiController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> Search(string q)
        {
            var clients = await _unitOfWork.Clients.Search(User.Identity.GetCompanyID(), q);

            return new ObjectResult(clients);
        }

        [HttpPost]
        public IActionResult AddClient([FromBody] ClientViewModel c)
        {
            var client = _mapper.Map<Client>(c);
            client.CompanyID = User.Identity.GetCompanyID();

            _unitOfWork.Clients.Add(client);

            _unitOfWork.Complete();

            return Ok(new {successMessage = "Client saved"});
        }
    }
}