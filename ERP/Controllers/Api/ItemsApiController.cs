using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels;
using ERP.ViewModels.ItemsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Api
{
    [Authorize]
    [Route("api/Items/[Action]")]
    [ApiController]
    public class ItemsApiController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ItemsApiController(IUnitOfWork unitOfWOrk, IMapper mapper)
        {
            _unitOfWork = unitOfWOrk;
            _mapper = mapper;
        }

        public async Task<IActionResult> Search(string q)
        {
            return new OkObjectResult(await _unitOfWork.Items.Search(User.Identity.GetCompanyID(), q));
        }

        public async Task<IActionResult> ItemTypes()
        {
            return new OkObjectResult(
                _mapper.Map<IEnumerable<KeyValue>>(
                    await _unitOfWork.ItemTypes.GetActiveItemTypes(User.Identity.GetCompanyID())));
        }

        public async Task<IActionResult> ItemPacking()
        {
            return new OkObjectResult(
                _mapper.Map<IEnumerable<KeyValue>>(
                    await _unitOfWork.ItemPacking.GetAllAsync(User.Identity.GetCompanyID())));
        }

        public async Task<IActionResult> VatClass()
        {
            return new OkObjectResult(_mapper.Map<IEnumerable<KeyValue>>(await _unitOfWork.VatClass
                .GetActiveVatClass(User.Identity.GetCompanyID())));
        }

        [HttpPost]
        public IActionResult SaveItem([FromBody] ItemViewModel model)
        {
            var userCompanyId = User.Identity.GetCompanyID();
            var price = new ItemPrice
            {
                Price = model.Price,
                PriceWithVAT = model.PriceWithVAT,
                DateValidFrom = model.DateValidFrom,
                DateValidTo = model.DateValidTo,
                CompanyID = userCompanyId,
                Item = new Item
                {
                    Code = model.Code,
                    Name = model.Name,
                    Description = model.Description,
                    Active = model.Active,
                    ItemTypeID = model.ItemTypeID,
                    PackingID = model.ItemPackageID,
                    VatClassID = model.VatClassID,
                    CompanyID = userCompanyId
                }
            };

            _unitOfWork.ItemPrices.Add(price);
            _unitOfWork.Complete();

            return Ok(new {successMessage = "Item saved"});
        }
    }
}