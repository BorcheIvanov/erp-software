using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels;
using ERP.ViewModels.DocumentsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers.Api
{
    [Authorize]
    [Route("api/Documents/[action]")]
    [ApiController]
    public class DocumentsApiController : ControllerBase
    {
        private readonly IUnitOfWork _unitOfWork;

        public DocumentsApiController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<IActionResult> DocumentTypesSelectList()
        {
            var documentTypes = (await _unitOfWork.DocumentTypes.GetActiveDocumentTypes(User.Identity.GetCompanyID()))
                .ToList()
                .Select(dt => new KeyValue {Key = dt.ID, Value = dt.Name});
            return new ObjectResult(documentTypes);
        }

        public async Task<IActionResult> AddItemToDocument([FromBody] DocumentDetailsViewModel dd)
        {
            var userCompanyId = User.Identity.GetCompanyID();
            var item = await _unitOfWork.Items.GetItemWithPrice(dd.ItemID, userCompanyId);

            if (item == null)
            {
                ModelState.AddModelError("", "There was Error");
                return BadRequest(ModelState);
            }

            if (item.ItemPrice.Price == 0)
            {
                ModelState.AddModelError("", "There is no valid price for this item");
                return BadRequest(ModelState);
            }

            var itemDetails = new DocumentDetailsViewModel
            {
                ItemCode = item.Code,
                ItemID = item.ID,
                ItemName = item.Name,
                BasicPrice = item.ItemPrice.Price,
                PriceID = item.ItemPrice.ID,
                VatClassID = item.VatClass.ID,
                VAT = item.VatClass.Value,
                Price = item.ItemPrice.Price + item.VatClass.Value / 100 * item.ItemPrice.Price,
                Quantity = dd.Quantity,
                TotalPrice = dd.Quantity * (item.ItemPrice.Price + item.VatClass.Value / 100 * item.ItemPrice.Price)
            };

            return new OkObjectResult(itemDetails);
        }

        public async Task<ActionResult> SaveDocument([FromBody] DocumentViewModel d)
        {
            try
            {
                var userCompanyId = User.Identity.GetCompanyID();
                var document = new Document
                {
                    DocumentDate = d.Date,
                    Year = d.Date.Year,
                    DocumentTypeID = d.DocumentTypeID,
                    CompanyID = userCompanyId,
                    ClientID = d.ClientID,
                    DocumentDetails = new List<DocumentDetails>()
                };

                foreach (var dd in d.DocumentDetails)
                {
                    var item = await _unitOfWork.Items.GetItemWithPrice(dd.ItemID, userCompanyId);

                    document.DocumentDetails.Add(new DocumentDetails
                    {
                        Document = document,
                        ClientID = d.ClientID,
                        ItemID = item.ID,
                        VatClassID = item.VatClass.ID,
                        ItemPriceID = item.ItemPrice.ID,
                        Quantity = dd.Quantity,
                        TotalBasicPrice = dd.Quantity * item.ItemPrice.Price,
                        TotalVatPrice = dd.Quantity * item.ItemPrice.Price * item.VatClass.Value / 100M,
                        TotalPrice = dd.Quantity * item.ItemPrice.Price +
                                     dd.Quantity * item.ItemPrice.Price * item.VatClass.Value /
                                     100M, // TotalBasicPrice + TotalVatPrice,
                        CompanyID = userCompanyId
                    });
                }

                document.BasicPrice = document.DocumentDetails.Sum(x => x.TotalBasicPrice);
                document.VatPrice = document.DocumentDetails.Sum(x => x.TotalVatPrice);
                document.Price = document.DocumentDetails.Sum(x => x.TotalPrice);

                _unitOfWork.Documents.Add(document);
                await _unitOfWork.CompleteAsync();

                return new ObjectResult(new {document.ID});
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("", "Error saving document");
                ModelState.AddModelError("", ex.Message);
                return BadRequest(ModelState);
            }
        }
    }
}