using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels.DocumentsViewModels;
using jsreport.AspNetCore;
using jsreport.Client;
using jsreport.Types;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace ERP.Controllers
{
    [Authorize]
    public class DocumentsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public DocumentsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["title"] = "Documents";

            return View(new IndexViewModel
            {
                DocumentsList =
                    _mapper.Map<IEnumerable<DocumentViewModel>>(
                        await _unitOfWork.Documents.GetAllDocumentsAsync(User.Identity.GetCompanyID()))
            });
        }

        public async Task<IActionResult> DocumentType(int? id)
        {
            var model = new DocumentTypeViewModel();

            model.Active = true;
            if (id != null)
                model = _mapper.Map<DocumentTypeViewModel>(
                    await _unitOfWork.DocumentTypes.GetByIDAsync((int) id, User.Identity.GetCompanyID()));

            model.DocumentTypeList = await GetDocumentTypesList();

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> DocumentType(DocumentTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var documentType = _mapper.Map<DocumentType>(model);
                documentType.CompanyID = User.Identity.GetCompanyID();
                _unitOfWork.DocumentTypes.AddOrUpdate(documentType);
                await _unitOfWork.CompleteAsync();
                return RedirectToAction("DocumentType");
            }

            model.DocumentTypeList = await GetDocumentTypesList();
            return View(model);
        }

        [HttpGet]
        [MiddlewareFilter(typeof(JsReportPipeline))]
        public IActionResult Invoice(int id)
        {
            HttpContext.JsReportFeature().Recipe(Recipe.PhantomPdf);
            return View(_unitOfWork.Documents.GetDocument(id, User.Identity.GetCompanyID()));
        }

        [HttpGet]
        public async Task<IActionResult> InvoiceAsync(int id)
        {
            var d = _unitOfWork.Documents.GetDocument(id, User.Identity.GetCompanyID());
            if (d == null) return NotFound();

            //TODO: refactor this
            var invoice = new InvoiceViewModel
            {
                DocumentNumber = $"{d.ID}-{d.DocumentTypeID}/{d.Year}",
                DocumentDate = d.DocumentDate.ToShortDateString(),
                DocumentDueDate = d.DocumentDate.AddDays(d.Client.DaysForPaying).ToShortDateString(),
                DocumentName = d.DocumentType.Name,
                CompanyLogo =
                    $"<img src='data:image/jpg;base64,{(d.Company.Logo == null ? string.Empty : Convert.ToBase64String(d.Company.Logo))}' alt='Company logo goes here.' width='150' height='100'/>",
                CompanyDetails =
                    $"{d.Company.Name}{(d.Company.UniqueNumber == null ? string.Empty : $"<br>{d.Company.UniqueNumber}")}{(d.Company.Address == null ? string.Empty : "<br>" + d.Company.Address)}{(d.Company.PhoneNumber == null ? string.Empty : "<br>" + d.Company.PhoneNumber)}{(d.Company.ZipCode == 0 ? string.Empty : "<br>" + d.Company.ZipCode)}{(d.Company.City == null ? string.Empty : "<br>" + d.Company.City)}{(d.Company.Country == null ? string.Empty : "<br>" + d.Company.Country)}",
                ClientDetails =
                    $"{d.Client.Name}{(d.Client.UniqueNumber == null ? string.Empty : $"<br>{d.Client.UniqueNumber}")}{(d.Client.Address == null ? string.Empty : "<br>" + d.Client.Address)}{(d.Client.PhoneNumber == null ? string.Empty : "<br>" + d.Client.PhoneNumber)}{(d.Client.ZipCode == 0 ? string.Empty : "<br>" + d.Client.ZipCode)}{(d.Client.City == null ? string.Empty : "<br>" + d.Client.City)}{(d.Client.Country == null ? string.Empty : "<br>" + d.Client.Country)}",
                BasicPrice = Math.Round(d.BasicPrice).ToString("N2"),
                VatPrice = Math.Round(d.VatPrice).ToString("N2"),
                Price = Math.Round(d.Price).ToString("N2"),
                DocumentDetails = d.DocumentDetails.Select(dd => new InvoiceDetailsViewModel
                {
                    ItemCode = dd.Item.Code ?? string.Empty,
                    ItemName = $"{dd.Item.Name} ({dd.Item.Description ?? string.Empty})",
                    ItemType = dd.Item.ItemType.Name ?? string.Empty,
                    ItemPacking = dd.Item.Packing.Name,
                    Quantity = dd.Quantity,
                    ItemBasicPrice = dd.ItemPrice.Price.ToString("N2"),
                    ItemVatClass = dd.VatClass.Value.ToString("N2"),
                    ItemPrice = Math.Round(dd.ItemPrice.Price + dd.ItemPrice.Price * dd.VatClass.Value / 100).ToString("N2"),
                    TotalBasicPrice = Math.Round(dd.TotalBasicPrice).ToString("N2"),
                    TotalVatPrice = Math.Round(dd.TotalVatPrice).ToString("N2"),
                    TotalPrice = Math.Round(dd.TotalPrice).ToString("N2")
                }).ToList()
            };

            var rs = new ReportingService("https://adaerp.jsreportonline.net", "borce.10712@gmail.com", "bokataa");
            var report = await rs.RenderByNameAsync("Invoice", invoice);

            return new FileStreamResult(report.Content, "application/pdf");
        }

        private async Task<List<DocumentTypeViewModel>> GetDocumentTypesList()
        {
            return _mapper.Map<List<DocumentTypeViewModel>>(
                await _unitOfWork.DocumentTypes.GetAllAsync(User.Identity.GetCompanyID()));
        }
    }
}