using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using ERP.DomainModel;
using ERP.DomainRepository;
using ERP.Extensions;
using ERP.ViewModels;
using ERP.ViewModels.ItemsViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.Controllers
{
    [Authorize]
    public class ItemsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public ItemsController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IActionResult> Index()
        {
            ViewData["title"] = "Items";

            return View(
                _mapper.Map<IEnumerable<ItemViewModel>>(
                    await _unitOfWork.Items.GetItemListWithPrices(User.Identity.GetCompanyID())));
        }

        public async Task<IActionResult> Item(int? id)
        {
            var item = new ItemViewModel();

            if (id != null)
                item = _mapper.Map<ItemViewModel>(
                    await _unitOfWork.Items.GetItemWithPrice((int) id, User.Identity.GetCompanyID()));

            item.ItemTypeList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.ItemTypes.GetActiveItemTypes(User.Identity.GetCompanyID())));
            item.ItemPackageList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.ItemPacking.GetAllAsync(User.Identity.GetCompanyID()))
            );
            item.VatClassList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.VatClass.GetActiveVatClass(User.Identity.GetCompanyID())));


            return View(item);
        }


        [HttpPost]
        public async Task<IActionResult> Item(ItemViewModel model)
        {
            if (model.Price <= 0)
            {
                ModelState.AddModelError(string.Empty, "Price should be greater than 0");
                return View(model);
            }

            model.ItemTypeList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.ItemTypes.GetActiveItemTypes(User.Identity.GetCompanyID())));

            model.ItemPackageList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.ItemPacking.GetAllAsync(User.Identity.GetCompanyID()))
            );

            model.VatClassList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(
                await _unitOfWork.VatClass.GetActiveVatClass(User.Identity.GetCompanyID())));


            if (ModelState.IsValid)
            {
                //TODO: refactor this AddOrUpdate
                var item = _unitOfWork.Items.Find(x => x.ID == model.ID && x.CompanyID == User.Identity.GetCompanyID())
                    .SingleOrDefault();
                if (item == null)
                    _unitOfWork.ItemPrices.Add(new ItemPrice
                    {
                        ID = model.ItemPriceID,
                        Price = model.Price,
                        PriceWithVAT = model.PriceWithVAT,
                        CompanyID = User.Identity.GetCompanyID(),
                        DateValidFrom = model.DateValidFrom,
                        DateValidTo = model.DateValidTo,
                        Item = new Item
                        {
                            ID = model.ID,
                            Code = model.Code,
                            Name = model.Name,
                            Description = model.Description,
                            Active = model.Active,
                            PackingID = model.ItemPackageID,
                            ItemTypeID = model.ItemTypeID,
                            VatClassID = model.VatClassID,
                            CompanyID = User.Identity.GetCompanyID()
                        }
                    });
                else
                {
                    item.Code = model.Code;
                    item.Name = model.Name;
                    item.Description = model.Description;
                    item.Active = model.Active;
                    item.PackingID = model.ItemPackageID;
                    item.ItemTypeID = model.ItemTypeID;
                    item.VatClassID = model.VatClassID;
                    item.CompanyID = User.Identity.GetCompanyID();
                }

                await _unitOfWork.CompleteAsync();

                model.IsNotNew = true;
                model.Message = new MessageViewModel
                {
                    Type = MessageViewModel.TypeSuccess,
                    Text = "Item saved"
                };
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> ItemTypes(int? id)
        {
            var model = new ItemTypeViewModel();
            if (id != null)
                model = _mapper.Map<ItemTypeViewModel>(
                    await _unitOfWork.ItemTypes.GetByIDAsync((int) id, User.Identity.GetCompanyID()));

            model.ItemTypeList =
                _mapper.Map<List<ItemTypeViewModel>>(
                    await _unitOfWork.ItemTypes.GetAllAsync(User.Identity.GetCompanyID()));

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ItemTypes(ItemTypeViewModel model)
        {
            if (ModelState.IsValid)
            {
                var itemType = _mapper.Map<ItemType>(model);
                itemType.CompanyID = User.Identity.GetCompanyID();

                _unitOfWork.ItemTypes.AddOrUpdate(itemType);

                await _unitOfWork.CompleteAsync();
                return RedirectToAction("ItemTypes");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> ItemPacking(int? id)
        {
            var model = new ItemPackingViewModel();

            if (id != null)
            {
                model = _mapper.Map<ItemPackingViewModel>(await _unitOfWork.ItemPacking.GetByIDAsync((int)id, User.Identity.GetCompanyID()));
            }

            model.ItemSingleUnitList = GetSelectList(_mapper.Map<IEnumerable<KeyValue>>(await _unitOfWork.ItemSingleUnit.GetAllAsync(User.Identity.GetCompanyID())));
            model.ItemPackingList = _mapper.Map<List<ItemPackingViewModel>>(await _unitOfWork.ItemPacking.GetItemPackingWithSingleUnit(User.Identity.GetCompanyID()));
            
            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ItemPacking(ItemPackingViewModel model)
        {
            if (ModelState.IsValid)
            {
                var itemPacking = _mapper.Map<ItemPacking>(model);
                itemPacking.CompanyID = User.Identity.GetCompanyID();

                if (!string.IsNullOrEmpty(model.ItemSingleUnitName))
                    itemPacking.SingleUnit = new ItemSingleUnit
                    {
                        Name = model.ItemSingleUnitName,
                        CompanyID = User.Identity.GetCompanyID()
            };
                else
                    itemPacking.SingleUnitID = model.ItemSingleUnitID;

                _unitOfWork.ItemPacking.Add(itemPacking);
                await _unitOfWork.CompleteAsync();
            }

            return RedirectToAction("ItemPacking");
        }

        [HttpGet]
        public async Task<IActionResult> VatClass(int? id)
        {
            var model = new VatClassViewModel();

            if (id != null)
                model = _mapper.Map<VatClassViewModel>(
                    await _unitOfWork.VatClass.GetByIDAsync((int) id, User.Identity.GetCompanyID()));

            model.VatClassList =
                _mapper.Map<List<VatClassViewModel>>(
                    await _unitOfWork.VatClass.GetAllAsync(User.Identity.GetCompanyID()));

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> VatClass(VatClassViewModel model)
        {
            if (ModelState.IsValid)
            {
                var vatClass = _mapper.Map<VatClass>(model);
                vatClass.CompanyID = User.Identity.GetCompanyID();
                _unitOfWork.VatClass.AddOrUpdate(vatClass);
                await _unitOfWork.CompleteAsync();
                return RedirectToAction("VatClass");
            }

            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> ItemPrices(int? id)
        {
            var model = new ItemPriceViewModel();
            
            if (id != null)
            {
                model = _mapper.Map<ItemPriceViewModel>(await _unitOfWork.Items.GetByIDAsync((int) id, User.Identity.GetCompanyID()));
                model.PriceList = _mapper.Map<List<ItemPriceViewModel>>(await _unitOfWork.ItemPrices.GetItemPriceList((int)id, User.Identity.GetCompanyID()));
            }

            model.ItemsList = _mapper.Map<List<ItemPriceViewModel>>(await _unitOfWork.Items.GetItemListWithPrices(User.Identity.GetCompanyID()));

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> ItemPrices(ItemPriceViewModel model)
        {
            if (ModelState.IsValid)
            {
                var price = _mapper.Map<ItemPrice>(model);
                price.CompanyID = User.Identity.GetCompanyID();
                _unitOfWork.ItemPrices.Add(price);
                await _unitOfWork.CompleteAsync();
            }

            return RedirectToAction("ItemPrices", new {id = model.ItemID});
        }

        public async Task<IActionResult> ItemStock()
        {
            ViewData["title"] = "Items stock";
            
            return View(_mapper.Map<IEnumerable<ItemStockViewModel>>(await _unitOfWork.Items.GetItemsStock(User.Identity.GetCompanyID())));
        }

        private static SelectList GetSelectList(IEnumerable enumerable) => new SelectList(enumerable, "Key", "Value");
    }
}