export interface DocumentDetails{
    itemID:number;
    itemCode:string;
    itemName:string;
    quantity:number;
    basicPrice:number;
    vat:number;
    vatClassID:number;
    priceID:number;
    price:number;
    totalPrice:number;
}