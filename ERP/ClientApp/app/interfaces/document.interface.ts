import { DocumentDetails } from "./documentDetails.interface";

export interface ErpDocument{
    id?:number,
    documentTypeID:number;
    clientID:number;
    clientName:string;
    date:Date;
    open:boolean;
    priceForPaying:number;

    documentDetails:DocumentDetails[];
}