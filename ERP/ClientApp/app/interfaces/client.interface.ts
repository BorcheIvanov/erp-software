export interface Client{
    name:string;
    daysForPaying:number;
    uniqueNumber : string;
    taxNumber: string;
    phoneNumber: string;
    address: string;
    city: string;
    zipCode:number
    country: string;
}