

export interface Item{
    id:number;
    name :string;
    code :string;
    description :string;
    active:boolean;
    
    itemTypeID:number;
    vatClassID:number;
    itemPackageID:number;

    price:number;
    dateValidFrom:Date;
    dateValidTo:Date;
    priceWithVAT:boolean;
}