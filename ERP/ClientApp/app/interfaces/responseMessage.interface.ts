export interface ResponseMessage{
    statusCode:number;
    success:boolean;
    values:string[];
}