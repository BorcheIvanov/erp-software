import { Component, OnInit } from '@angular/core';
import { ItemService } from '../../services/item.service';
import { DocumentService } from '../../services/document.service';

import  {Item} from '../../interfaces/Item.interface';
import { SelectList } from '../../interfaces/selectList.interface';
import { ClientService } from '../../services/client.service';
import { ErpDocument } from '../../interfaces/document.interface';
import { DocumentDetails } from '../../interfaces/documentDetails.interface';
import { ResponseMessage } from '../../interfaces/responseMessage.interface';
import { FunctionsService } from '../../services/functions.service';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
    items : Item[] = [];
    documentTypes : SelectList[] = []; 
    clients : any = [];
    itemResponseMessage: ResponseMessage = this.func.getEmptyResponseMessage();
    docResponseMessage: ResponseMessage = this.func.getEmptyResponseMessage();
  
    doc : ErpDocument = this.newDocumentObject();
    docDetails : DocumentDetails = {} as DocumentDetails;
    

    constructor(private itemService : ItemService
                ,private documentService : DocumentService
                ,private clientService:ClientService  
                ,private func:FunctionsService) {
    }

    ngOnInit(){
        this.documentService.getDocumentType()
                            .subscribe((res:SelectList[]) => {
                                this.documentTypes = res;
                            });
    }

    search(searchText:string){
        this.itemService.searchItems(searchText)
                        .subscribe((res:Item[]) => {
                            this.items = res;
                        });    
    }

    searchClient(searchClient:string){
        this.clientService.search(searchClient)
                            .subscribe((res:any) => {
                                this.clients = res;
                            });
    }

    selectItem(i:Item){
        if (i) {
            // this.selectedItem = i.code + ' ' + i.name + ' (' + i.description + ')'; 
            this.docDetails.itemName = i.code + ' ' + i.name + ' (' + i.description + ')';
            this.docDetails.itemID = i.id;
            this.items = [];
            (<HTMLInputElement>document.getElementById('quantityy')).focus();
        }
    }

    selectClient(c:any){
        if (c) {
            // this.selectedClient = c.name; 
            this.doc.clientName  = c.name;
            this.doc.clientID = c.id;
            this.clients = [];
            (<HTMLInputElement>document.getElementById('documentType')).focus();
        }
    }

    addItem(quantity:number){
        this.func.getEmptyResponseMessage();

        this.docDetails.quantity = quantity;
        this.documentService.addItemToDocument(this.docDetails)
                            .subscribe((res:DocumentDetails) => {
                                this.doc.documentDetails.push(res);
                                this.itemResponseMessage = this.func.getSuccesResponseMessageFromString('\'' + this.docDetails.itemName + '\' added');
                                console.log(res);
                                this.docDetails.itemID = 0;
                                this.docDetails.itemName = '';
                                this.docDetails.quantity = 0;
                            },
                            (err:any) => {
                                this.itemResponseMessage = this.func.getErrorResponseMessage(err);
                                console.log(this.itemResponseMessage);
                            },
                            () => {
                                this.sumOfTotals();
                                (<HTMLInputElement>document.getElementById('item')).focus();
                            });
    }

    removeItem(docItem:DocumentDetails){
        this.doc.documentDetails = this.doc.documentDetails.filter(item => item !== docItem);
        this.sumOfTotals();
    }

    sumOfTotals()
    {
        this.doc.priceForPaying = 0;
        this.doc.documentDetails.forEach(element => {
            this.doc.priceForPaying += element.totalPrice;
        });
    }

    saveDocument()
    {
        console.log('saving documetn');
        console.log(this.doc);
        this.docResponseMessage = this.func.getEmptyResponseMessage();
        this.documentService.saveDocument(this.doc)
                            .subscribe((res:any) => {
                                this.docResponseMessage = this.func.getSuccesResponseMessageFromString('Document saved');
                                this.doc.id = res.id;                         },
                            (err:any) => {
                                this.docResponseMessage = this.func.getErrorResponseMessage(err);
                            })
    }

    newDocument(){
        this.doc = this.newDocumentObject();
        this.itemResponseMessage = this.func.getEmptyResponseMessage();
        this.docResponseMessage = this.func.getEmptyResponseMessage();
    }

    private newDocumentObject(){
        return {
            id:undefined,
            documentTypeID : 0,
            clientID : 0,
            clientName : '',
            priceForPaying : 0,
            open:false,
            date : new Date(Date.now()),
            documentDetails : [] as DocumentDetails[]
        } as ErpDocument;
    }

    

}
