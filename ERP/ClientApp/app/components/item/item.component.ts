import {Component, OnInit} from '@angular/core';


import { SelectList } from '../../interfaces/selectList.interface';
import { ItemService } from '../../services/item.service';
import { Item } from '../../interfaces/Item.interface';
import { ResponseMessage } from '../../interfaces/responseMessage.interface';
import { FunctionsService } from '../../services/functions.service';

@Component({
    selector:'item',
    templateUrl:'./item.component.html',
    styleUrls:['./item.component.css']
})
export class ItemComponent implements OnInit{

    itemTypes:SelectList[] = [] as SelectList[];
    itemPacking:SelectList[] = [] as SelectList[];
    vatClass:SelectList[] = [] as SelectList[];
    responseMessage: ResponseMessage = this.func.getEmptyResponseMessage();

    item:Item = {} as Item;

    constructor(private itemService : ItemService, private func:FunctionsService) {
    }

    ngOnInit(){
        this.item = this.newItem();
    }

    newItem(): Item {
        var d = new Date();
        return {
            dateValidFrom : new Date(Date.now()),
            dateValidTo : new Date(d.getFullYear() + 1, d.getMonth(), d.getDate()),
            price : 0,
            active : true,
        } as Item;
    }

    getItemPacking(){
        this.itemService.itemPacking()
                        .subscribe((res:SelectList[]) => this.itemPacking = res,
                                    (err) => console.error(err))
    }

    getItemTypes(){
        this.itemService.itemTypes()
        .subscribe((res:SelectList[]) => this.itemTypes = res,
                    (err) => console.error(err))
    }

    getVatClass(){
        this.itemService.vatClass()
                        .subscribe((res:SelectList[]) => this.vatClass = res,
                                    (err) => console.error(err))
    }

    popUpItem(display:boolean, e:any){
        console.log(e);
        if (display && e.clientX > 0) {
            (<HTMLInputElement>document.getElementById('itemModal')).style.display='block';
            this.getItemPacking();
            this.getItemTypes();
            this.getVatClass();

            this.responseMessage = this.func.getEmptyResponseMessage();
        }
        else {
            (<HTMLInputElement>document.getElementById('itemModal')).style.display='none';
            this.item = {} as Item;
        }
    }

    saveItem(){
        console.log(this.item);

        this.responseMessage = this.func.getEmptyResponseMessage();

        this.itemService.saveItem(this.item)
                        .subscribe((res:any) => {
                                        this.responseMessage = this.func.getSuccesResponseMessage(res);
                                    },
                                    (err:any) => {
                                        this.responseMessage = this.func.getErrorResponseMessage(err);
                                        console.log(this.responseMessage);
                                    },
                                    () => {
                                        console.log(this.responseMessage);
                                    });
    }

}