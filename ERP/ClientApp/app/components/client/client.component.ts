import { Component, OnInit } from '@angular/core';
// import { NgForm } from '@angular/forms';
import { Client } from '../../interfaces/client.interface';
import { ClientService } from '../../services/client.service';
import { ResponseMessage } from '../../interfaces/responseMessage.interface';
import { forEach } from '@angular/router/src/utils/collection';
import { FunctionsService } from '../../services/functions.service';

@Component({
    selector: 'client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

    client: Client = {} as Client;
    responseMessage: ResponseMessage = this.func.getEmptyResponseMessage();

    constructor(private clientService:ClientService, private func:FunctionsService){

    }

    ngOnInit(){

    }

    onSubmit(){
        this.responseMessage = this.func.getEmptyResponseMessage();

        this.clientService.addClient(this.client)
                            .subscribe((res:any) => {
                                            this.responseMessage = this.func.getSuccesResponseMessage(res);
                                        },
                                        (err:any) => {
                                            this.responseMessage = this.func.getErrorResponseMessage(err);
                                            console.log(this.responseMessage);
                                        },
                                        () => {
                                            console.log(this.responseMessage);
                                        });
    }

    
    popUpClient(display:boolean, e:any){
        if (display && e.clientX > 0) {
            (<HTMLInputElement>document.getElementById('clientModal')).style.display='block';
            this.client = {} as Client;
            this.responseMessage = this.func.getEmptyResponseMessage();
        }
        else {
            (<HTMLInputElement>document.getElementById('clientModal')).style.display='none';
        }
    }
}
