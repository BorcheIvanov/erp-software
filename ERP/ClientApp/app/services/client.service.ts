import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { Client } from '../interfaces/client.interface';

@Injectable()

export class ClientService{
    /**
     *
     */
    constructor(private http:HttpClient, @Inject('BASE_URL') private url:string) {
    }

    search(text:string){
        return this.http.get(this.url + 'api/clients/search?q=' + text);
    }

    addClient(client:Client){
        return this.http.post(this.url + 'api/clients/addClient', client);
    }
}