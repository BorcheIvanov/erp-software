import { Injectable } from "@angular/core";
import { ResponseMessage } from "../interfaces/responseMessage.interface";

@Injectable()

export class FunctionsService{

    getEmptyResponseMessage(): ResponseMessage{
        return {
            success: false, 
            values: [] as string[]
        } as ResponseMessage;
    }

    getSuccesResponseMessageFromString(text:string):ResponseMessage{
        let rm:ResponseMessage = this.getEmptyResponseMessage();
        rm.success = true;
        rm.statusCode = 200;
        rm.values.push(text);
        return rm;
    }

    getSuccesResponseMessage(res:any):ResponseMessage{
        let rm:ResponseMessage = this.getEmptyResponseMessage();
        rm.success = true;
        rm.statusCode = 200;
        rm.values.push(res.successMessage);
        return rm;
    }

    getErrorResponseMessage(err:any):ResponseMessage{
        let rm:ResponseMessage = this.getEmptyResponseMessage();
        rm.success = err.ok;
        rm.statusCode = err.status;
        if (err.status === 400) {
            var tempArray:any = (<any>Object).values(err.error);
            for (let index = 0; index < tempArray.length; index++) {
                for(let j = 0; j < tempArray[index].length; j++)
                rm.values.push(tempArray[index][j]);
            }
        } else {
            rm.values.push("Something went wrong!");
        }

        return rm;
    }

}