import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

import  {Item} from '../interfaces/Item.interface';
import { SelectList } from '../interfaces/selectList.interface';


@Injectable()

export class ItemService{
    
    constructor(private http : HttpClient, @Inject('BASE_URL') private url: string) { }

    searchItems(text:string):Observable<Item[]>{
        return this.http.get<Item[]>(this.url + 'api/items/search?q=' + text);
    }

    itemTypes():Observable<SelectList[]>{
        return this.http.get<SelectList[]>(this.url + 'api/items/itemTypes');
    }

    itemPacking():Observable<SelectList[]>{
        return this.http.get<SelectList[]>(this.url + 'api/items/itemPacking');
    }

    vatClass():Observable<SelectList[]>{
        return this.http.get<SelectList[]>(this.url + 'api/items/vatClass');
    }

    saveItem(item:Item){
        return this.http.post("api/items/saveItem", item);
    }
}
