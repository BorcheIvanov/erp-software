import {Injectable, Inject} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { SelectList } from '../interfaces/selectList.interface';

import { DocumentDetails } from '../interfaces/documentDetails.interface';
import { ErpDocument } from '../interfaces/document.interface';

@Injectable()

export class DocumentService{
    
    constructor(private http : HttpClient, @Inject('BASE_URL') private url: string) { }

    getDocumentType():Observable<SelectList[]>{
        return this.http.get<SelectList[]>(this.url + 'api/Documents/DocumentTypesSelectList');
    }

    addItemToDocument(documentDetails:DocumentDetails):Observable<DocumentDetails>{
        return this.http.post<DocumentDetails>(this.url + 'api/documents/addItemToDocument', documentDetails);
    }

    saveDocument(doc: ErpDocument) {
        return this.http.post(this.url + 'api/documents/saveDocument', doc);
    }

}

