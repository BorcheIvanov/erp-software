import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import 'rxjs';

import { AppComponent } from './components/app/app.component';
import { ItemService } from './services/item.service';
import { DocumentService } from './services/document.service';
import { ClientService } from './services/client.service';
import { ClientComponent } from './components/client/client.component';
import { ItemComponent } from './components/item/item.component';
import { FunctionsService } from './services/functions.service';

@NgModule({
    declarations: [
        AppComponent,
        ClientComponent,
        ItemComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            // { path: 'home', component: HomeComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ],
    providers:[
        ItemService,
        DocumentService,
        ClientService,
        FunctionsService
    ]
})
export class AppModuleShared {
}
