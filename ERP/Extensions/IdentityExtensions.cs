using System;
using System.Security.Claims;
using System.Security.Principal;

namespace ERP.Extensions
{
    public static class IdentityExtensions
    {
        public static int GetCompanyID(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity) identity).FindFirst("CompanyID");

            if (claim == null)
                throw new Exception("No CompanyID assigned!");

            if (!int.TryParse(claim.Value, out int i))
                throw new Exception("CompanyID is not a number!");

            return int.Parse(claim.Value);
        }
    }
}