using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.DocumentsViewModels
{
    public class DocumentTypeViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public bool AffectingQuantity { get; set; }
        [Display(Name = "Direction")]
        [Required]
        public int? AffectingValue { get; set; }
        public bool Active { get; set; }
        public bool IsNotNew { get; set; }
        public List<DocumentTypeViewModel> DocumentTypeList { get; set; }

        public DocumentTypeViewModel()
        {
            DocumentTypeList = new List<DocumentTypeViewModel>();
        }
    }
}