using System;
using System.Collections.Generic;

namespace ERP.ViewModels.DocumentsViewModels
{
    public class DocumentViewModel
    {
        public int ID { get; set; }
        public string DocumentNumber { get; set; }
        public int ClientID { get; set; }
        public string ClientName { get; set; }
        public int DocumentTypeID { get; set; }
        public string DocumentTypeName { get; set; }
        public DateTime Date { get; set; }
        public DateTime DateForPayingDue { get; set; }
        public bool OpenDocument { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal VatPrice { get; set; }
        public decimal Price { get; set; }
        public List<DocumentDetailsViewModel> DocumentDetails { get; set; }

        public DocumentViewModel()
        {
            DocumentDetails = new List<DocumentDetailsViewModel>();
        }
    }
}