using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.DocumentsViewModels
{
    public class DocumentDetailsViewModel
    {
        [Required(ErrorMessage = "Please select item")]
        public int ItemID {get;set;}
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public int VatClassID { get; set; }
        public decimal VAT { get; set; }
        public int PriceID { get; set; }
        public decimal BasicPrice { get; set; }
        public decimal Price { get; set; }
        [Required]
        [Range(0.0001, int.MaxValue, ErrorMessage = "Quantity should be greater than 0")]
        public decimal Quantity { get; set; }
        public decimal TotalPrice { get; set; }
       
    }
}