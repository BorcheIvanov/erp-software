namespace ERP.ViewModels.DocumentsViewModels
{
    public class InvoiceDetailsViewModel
    {
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public string ItemType { get; set; }
        public string ItemPacking { get; set; }
        public decimal Quantity { get; set; }
        public string ItemBasicPrice { get; set; }
        public string ItemVatClass { get; set; }
        public string ItemPrice { get; set; }
        public string TotalBasicPrice { get; set; }
        public string TotalVatPrice { get; set; }
        public string TotalPrice { get; set; }
    }
}