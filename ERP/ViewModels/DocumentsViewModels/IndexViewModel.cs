using System.Collections.Generic;

namespace ERP.ViewModels.DocumentsViewModels
{
    public class IndexViewModel
    {
        public IEnumerable<DocumentViewModel> DocumentsList { get; set; }
    }
}