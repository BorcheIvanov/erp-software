using System.Collections.Generic;

namespace ERP.ViewModels.DocumentsViewModels
{
    public class InvoiceViewModel
    {
        public string DocumentNumber { get; set; }
        public string DocumentDate { get; set; }
        public string DocumentDueDate { get; set; }
        public string DocumentName { get; set; }
        public string CompanyLogo { get; set; }
        public string CompanyDetails { get; set; }
        public string ClientDetails { get; set; }
        public string BasicPrice { get; set; }
        public string VatPrice { get; set; }
        public string Price { get; set; }
        public List<InvoiceDetailsViewModel> DocumentDetails { get; set; }

        public InvoiceViewModel()
        {
            DocumentDetails = new List<InvoiceDetailsViewModel>();
        }
    }
}