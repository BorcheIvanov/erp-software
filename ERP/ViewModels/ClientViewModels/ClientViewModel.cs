
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.ClientsViewModels
{
    public class ClientViewModel
    {
        public int ID { get; set; }
        [Display(Name = "Client Name")]
        [Required]
        public string Name { get; set; }
        
        [Display(Name = "Days for paying")]
        [Range(0, int.MaxValue, ErrorMessage = "Days number must be greater than 0")]
        [Required]  
        public int DaysForPaying { get; set; }
        [Display(Name = "Unique number")]
        public string UniqueNumber { get; set; }
        [Display(Name = "Tax number")]
        public string TaxNumber { get; set; }
        public string Address {get;set;}
        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }
        [Display(Name = "Zip code")]
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public List<ClientViewModel> ClientList { get; set; }
        public bool IsNotNew { get; set; }
        public bool Active { get; set; }

        public ClientViewModel()
        {
            ClientList = new List<ClientViewModel>();
        }
    }
}