namespace ERP.ViewModels
{
    public class MessageViewModel
    {

        public static string TypeSuccess = "success";
        public static string TypeDanger = "danger";
        public static string TypeInfo = "info";


        public string Type { get; set; }
        public string Text { get; set; }
    }
}