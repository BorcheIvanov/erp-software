﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ERP.ViewModels
{
    public class KeyValue
    {
        public int Key { get; set; }
        public string Value { get; set; }
    }
}
