using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.ViewModels.AdministrationViewModels
{
    public class UsersViewModel{
        public string ID { get; set; }
        [Display(Name="User name")]
        public string UserName { get; set; }

        public bool IsNotNew { get; set; }
        public bool Active { get; set; }
        public string Role { get; set; }
        public SelectList RolesList { get; set; }
        public List<UsersViewModel> List { get; set; }
    }
}