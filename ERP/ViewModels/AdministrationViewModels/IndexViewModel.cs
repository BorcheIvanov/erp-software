using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace ERP.ViewModels.AdministrationViewModels
{
    public class IndexViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name="Company name")]
        public string Name { get; set; }
        public string UniqueNumber { get; set; }
        public string TaxNumber { get; set; }
        public string Address {get;set;}
        public string PhoneNumber { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        [Display(Name = "Upload Logo")]
        public IFormFile LogoFile { get; set; }
        public byte[] Logo { get; set; }
        public string LogoSrc { get; set; }
        public UsersViewModel User { get; set; }
    }
}