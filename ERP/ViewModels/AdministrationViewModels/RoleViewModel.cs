
namespace ERP.ViewModels.AdministrationViewModels
{
    public class RoleViewModel
    {
        public int ID { get; set; }
        public string Role { get; set; }
    }
}