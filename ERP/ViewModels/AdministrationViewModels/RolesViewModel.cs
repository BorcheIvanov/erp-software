
using System.Collections.Generic;

namespace ERP.ViewModels.AdministrationViewModels
{
    public class RolesViewModel
    {
        public RolesViewModel()
        {
            Roles = new List<RoleViewModel>();
        }

        public string Role { get; set; }

        public List<RoleViewModel> Roles {get;set;}
    }
}