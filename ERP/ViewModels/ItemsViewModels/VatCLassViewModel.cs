using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.ItemsViewModels
{
    public class VatClassViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(0.0001, 100)]
        public decimal? Value { get; set; }
        public bool Active { get; set; }
        public bool IsNotNew { get; set; }
        public List<VatClassViewModel> VatClassList { get; set; }

        public VatClassViewModel()
        {
            VatClassList = new List<VatClassViewModel>();
        }
    }
}