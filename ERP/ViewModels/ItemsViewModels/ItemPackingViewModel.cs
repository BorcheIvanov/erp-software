
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.ViewModels.ItemsViewModels
{
    public class ItemPackingViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int ItemSingleUnitID { get; set; }
        public SelectList ItemSingleUnitList { get; set; }
        public string ItemSingleUnitName { get; set; }
        public List<ItemPackingViewModel> ItemPackingList { get; set; }

        public ItemPackingViewModel()
        {
            ItemPackingList = new List<ItemPackingViewModel>();
        }
    }
}