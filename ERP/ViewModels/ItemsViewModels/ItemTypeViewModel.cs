using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.ItemsViewModels
{
    public class ItemTypeViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }

        [Display(Name = "Item is affecting quantity")]
        public bool AffectingQuantity{get;set;}
        public bool Active { get; set; }
        public bool IsNotNew { get; set; }
        public List<ItemTypeViewModel> ItemTypeList { get; set; }

        public ItemTypeViewModel()
        {
            ItemTypeList = new List<ItemTypeViewModel>();
            Active = true;
        }
    }
}