namespace ERP.ViewModels.ItemsViewModels
{
    public class ItemStockViewModel
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Stock { get; set; }
    }
}