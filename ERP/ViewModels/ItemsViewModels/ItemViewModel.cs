using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ERP.ViewModels.ItemsViewModels
{
    public class ItemViewModel
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public bool Active { get; set; } = true;
        [Required]
        public int ItemTypeID { get; set; }
        public string ItemType { get; set; }
        public SelectList ItemTypeList{get;set;}
        [Required]
        public int VatClassID { get; set; }
        public string VatClassName { get; set; }
        public SelectList VatClassList { get; set; }
        [Required]
        public int ItemPackageID { get; set; }
        public SelectList ItemPackageList { get; set; }
        public int ItemPriceID { get; set; }
        [Range(0.0001, int.MaxValue, ErrorMessage = "Price should be greater than 0")]
        public decimal Price { get; set; } = 0;
        [DataType(DataType.Date)]
        public DateTime DateValidFrom { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime DateValidTo { get; set; } = DateTime.Now.AddYears(1);
        public bool PriceWithVAT { get; set; }

        public bool IsNotNew { get; set; } = false;
        public MessageViewModel Message {get;set;}

    }
}