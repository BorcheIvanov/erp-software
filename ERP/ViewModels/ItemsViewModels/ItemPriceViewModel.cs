using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace ERP.ViewModels.ItemsViewModels
{
    public class ItemPriceViewModel
    {
        public int ID { get; set; }
        public int? ItemID { get; set; }
        public string ItemName { get; set; }
        public decimal Price { get; set; }
        [DataType(DataType.Date)]
        public DateTime DateValidFrom { get; set; } = DateTime.Now;
        [DataType(DataType.Date)]
        public DateTime DateValidTo { get; set; } = DateTime.Now.AddYears(1);
        public bool PriceWithVAT { get; set; }
        public List<ItemPriceViewModel> ItemsList { get; set; }
        public List<ItemPriceViewModel> PriceList { get; set; }
    
        public ItemPriceViewModel()
        {
            ItemsList = new List<ItemPriceViewModel>();
            PriceList = new List<ItemPriceViewModel>();
        }
    }
}